<?php

namespace XLabs\ChatBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XLabs\ChatBundle\Entity\XLabsChatUser;
use \DateTime;

class XLabsChatUserStatus
{
    private $container;
    private $em;
    private $token_storage;
    private $xlabs_chat_config;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
        $this->token_storage = $container->get('security.token_storage');
        $this->xlabs_chat_config = $container->getParameter('xlabs_chat_config');
    }
    
    public function onLogin(Event $event)
    {
        $params = $event->getParams();
        $user_id = $params['user_id'];

        $xlc_user = $this->em->getRepository('XLabsChatBundle:XLabsChatUser')->getXLabsChatUser($user_id);
        if(!$xlc_user)
        {
            $xlc_user = new XLabsChatUser();
            $xlc_user->setUser($this->em->getReference($this->xlabs_chat_config['user_entity'], $user_id));
        }

        $xlc_user->setStatus(XLabsChatUser::XLABS_CHAT_USER_STATUS_ONLINE);
        $xlc_user->setLastLogin(new DateTime());
        $this->em->persist($xlc_user);
        $this->em->flush();

        //mail('xavi.mateos@manicamedia.com', 'XLabs Chat User LOGIN '.$user_id, '');
    }

    public function onLogout(Event $event)
    {
        $params = $event->getParams();
        $user_id = $params['user_id'];

        $xlc_user = $this->em->getRepository('XLabsChatBundle:XLabsChatUser')->getXLabsChatUser($user_id);
        if(!$xlc_user)
        {
            $xlc_user = new XLabsChatUser();
            $xlc_user->setUser($this->em->getReference($this->xlabs_chat_config['user_entity'], $user_id));
        }

        $xlc_user->setStatus(XLabsChatUser::XLABS_CHAT_USER_STATUS_OFFLINE);
        $this->em->persist($xlc_user);
        $this->em->flush();

        //mail('xavi.mateos@manicamedia.com', 'XLabs Chat User LOGOUT '.$user_id, '');
    }
}