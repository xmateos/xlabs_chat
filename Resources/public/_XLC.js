// XLabsChatRequest (XCR)
var _XLC = _XLC || (function(){
    var _this;
    return {
        ownerDocument: document,
        original_title: document.title,
        _this: this,
        init: function() {
            _this = this;
        },
        user_entity_mappings: {
            id: false,
            username: false,
            avatar: false
        },
        conversations: '{}', // json with existing conversations
        users: {}, // all users
        online_users: {}, // online users
        user: {},
        blocked: [], // blocked users
        socket: false,
        settings: {
            idle_counter: 0,
            idle_time: 120,
            idle_status_checked: false,
            typing_timer: false,
            typing_time: 1000,
            typing_status_checked: false,
            max_file_size: 1048576, // 1 Mb default
            images_prefix: false,
            no_focus_new_message_timer: false,
            no_focus_new_message_title: 'You have new message(s)',
            no_focus_new_message_time: 700
        },
        window_focus: true,
        conversation: this.conversation || (function(){
            return {
                conversation_id: false,
                title: '',
                participants: [],
                unread_messages: 0,
                conversation_type: false,
                avatar: false,
                last_message: false
            };
        }),
        message: this.message || (function(){
            return {
                conversation_id: false,
                sender_id: false,
                participants: [],
                body: '',
                timestamp: Math.floor(Date.now() / 1000)
            };
        }),
        _set: function(param, value){
            this[param] = $.extend({}, this[param], value);
        },
        liveNotification: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                title: false,
                msg: '',
                icon: false,
                muted: false,
                link: false,
                sound: false,
                fadeout_after_ms: false
                //onClick: ''
            }, data);
            //data.destinatary_id = data.destinatary_id ? md5(data.destinatary_id.toString()) : false;
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('notification', data) : false;
        },
        liveSound: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                file: ''
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('play_sound', data) : false;
        },
        liveSpeak: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                text: 'This is an intentional sample text'
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('speak_text', data) : false;
        },
        customMessage: function(data)
        {
            data = $.extend({
                destinatary_id: false,
                callback: ''
            }, data);
            data.destinatary_id = data.destinatary_id ? data.destinatary_id : false;
            'socket' in _this && _this.socket ? _this.socket.emit('custom_message', data) : false;
        },
        // event test
        /*events: {
            onConversationWindowLoad: new CustomEvent('onConversationWindowLoad', {
                    detail: {
                        message: "Hello World!",
                        time: new Date(),
                    },
                    bubbles: true,
                    cancelable: true
                }
            )
        }*/
    };
});

/*var _XCR_conversation = _XCR_conversation || (function(){
    return {
        conversation_id: false,
        title: '',
        participants: []
    };
});*/