// Player for audio notifications
window.addEventListener('load', function(){
    var xlabs_notification_player_wrapper = document.createElement('div');
    xlabs_notification_player_wrapper.setAttribute('id', 'xlabs_chat_player_wrapper');
    xlabs_notification_player_wrapper.innerHTML = '<audio id="xlabs_chat_player"><source id="mp3" src="" type="audio/mpeg" /><embed hidden="true" autostart="false" loop="false" src="" /></audio>';
    document.body.appendChild(xlabs_notification_player_wrapper);

    if(!XLC.socket)
    {
        console.log('Chat server is not running');
    }
});

// Check window focus to know whether to show notification or not
$(window).focus(function() {
    if(typeof XLC !== 'undefined')
    {
        XLC.window_focus = true;
        document.title = XLC.original_title;
        clearInterval(XLC.settings.no_focus_new_message_timer);
        XLC.settings.no_focus_new_message_timer = false;
    }
}).blur(function() {
    if(typeof XLC !== 'undefined')
    {
        XLC.window_focus = false;
    }
});

$(document).ready(function(){
    // Tell everybody I´m around
    XLC.socket.emit('login', XLC.user);

    // Load conversations
    if(XLC.conversations.length)
    {
        $.each(XLC.conversations, function(i, conversation){
            //console.log(conversation);
            var XLC_conversation = XLC.conversation();
            XLC_conversation.conversation_id = conversation.conversation_id;
            XLC_conversation.title = conversation.title;
            XLC_conversation.participants = conversation.participants.map(function(participant){
                return parseInt(participant.id);
            }).sort(function(a, b){return a-b});
            XLC_conversation.unread_messages = parseInt(conversation.total_unread_messages);
            XLC_conversation.conversation_type = conversation.conversation_type;
            XLC_conversation.last_message = ('messages' in conversation) && conversation.messages.length ? conversation.messages[0].body : false;

            if(conversation.conversation_type == 'user')
            {
                // it´s a private chat, then title is the username not matching the current user
                var other_user = conversation.participants.filter(function(participant){
                    return (participant.id != XLC.user.id);
                })[0];
                XLC_conversation.title = other_user.username;

                // Search in users array to get avatar
                XLC_conversation.avatar = ((other_user.id in XLC.users) && XLC.users[other_user.id].avatar) ? XLC.users[other_user.id].avatar : false;

                // dont create conversation if user is banned
                if((other_user.id in XLC.users) && XLC.users[other_user.id].banned)
                {
                    return true;
                }
            } else if(conversation.conversation_type == 'group') {
                // grab the group title
            }

            createConversationItem(XLC_conversation);
        });
    } else {
        $('.gqNoConvo').show();
    }

    // Load online users
    if(!$.isEmptyObject(XLC.online_users.online_user_ids))
    {
        var results = $.map(XLC.online_users.online_user_ids, function(value, index) {
            return [value];
        });
        for(var i=0; i<results.length; i++)
        {
            createOnlineUserItem(results[i]);
        }
        $('#onlineUsersList span.no_results_found').hide();

        // Check results online status
        XLC.socket.emit('check_user_status', XLC.user, results.map(function(user_id){
            return user_id;
        }));
    } else {
        $('#onlineUsersList span.no_results_found').show();
    }
});

// Create conversation on user click
$('body').on('click', '#userList li.gqOnlineUser', function(){
    var user_id = $(this).attr('data-user');

    var XLC_conversation = new XLC.conversation();
    XLC_conversation.conversation_id = getConversationId([user_id, XLC.user.id]);
    XLC_conversation.title = XLC.users[user_id].username;
    XLC_conversation.participants = sortParticipants([user_id, XLC.user.id]);
    XLC_conversation.conversation_type = 'user';
    XLC_conversation.avatar = ((user_id in XLC.users && XLC.users[user_id].avatar)) ? XLC.users[user_id].avatar : false;

    $('#conversationList').show();
    $('.gqNoConvo').hide();
    $('#userList').hide();
    $('input#onlineSearch').val('');

    var conversation_already_exists = createConversationItem(XLC_conversation);
    var conversationItem = getConversationItem(XLC_conversation.conversation_id);
    //conversationItem.attr('data-user', user_id);
    //var conversationItem = $('#conversationList li[data-conversation="' + XLC_conversation.conversation_id + '"]');
    //if(conversation_already_exists)
    //{
    //    conversationItem.attr('data-loaded', 'true');
    //}
    conversationItem.click();
});

// Create conversation on online user click
$('body').on('click', '#onlineUsersList li.gqOnlineUser', function(){
    var user_id = $(this).attr('data-user');

    var XLC_conversation = new XLC.conversation();
    XLC_conversation.conversation_id = getConversationId([user_id, XLC.user.id]);
    XLC_conversation.title = XLC.users[user_id].username;
    XLC_conversation.participants = sortParticipants([user_id, XLC.user.id]);
    XLC_conversation.conversation_type = 'user';
    XLC_conversation.avatar = ((user_id in XLC.users && XLC.users[user_id].avatar)) ? XLC.users[user_id].avatar : false;

    $('a[href="#tab-recent-conversations"]').trigger('click');
    $('#conversationList').show();
    $('.gqNoConvo').hide();
    $('#userList').hide();
    $('input#onlineSearch').val('');

    var conversation_already_exists = createConversationItem(XLC_conversation, true);
    var conversationItem = getConversationItem(XLC_conversation.conversation_id);
    //conversationItem.attr('data-user', user_id);
    //var conversationItem = $('#conversationList li[data-conversation="' + XLC_conversation.conversation_id + '"]');
    //if(conversation_already_exists)
    //{
    //    conversationItem.attr('data-loaded', 'true');
    //}
    conversationItem.click();
});

// Load history on conversation click
$('body').on('click', '#conversationList li.gqOnlineUser', function(){
    let conversationItem = $(this);
    let conversation_id = conversationItem.attr('data-conversation');

    // Highlight conversation on the left
    $('#conversationList li').removeClass('active');
    conversationItem.addClass('active');

    // Show conversation window
    $('#chatContainer .gqWhisperContainer').removeClass('active');
    activateConversationWindow({
        conversation_id: conversation_id,
        participants: conversationItem.attr('data-participants')
    });

    // Load messages if not loaded already
    if(conversationItem.attr('data-loaded') == 'false')
    {
        $.ajax({
            url: Routing.generate('xlabs_chat_get_conversation_messages'),
            data: {
                conversation_id: conversation_id
            },
            type: 'post',
            dataType: 'json',
            success: function(messages){
                var total_messages = messages.total_messages;
                $.each(messages.messages.reverse(), function(i, message){
                    appendMessageToConversationWindow(conversation_id, message);
                });
                conversationItem.attr('data-loaded', 'true');

                var conversationWindow = getConversationWindow(conversation_id);
                var total_shown_messages = conversationWindow.find('.gqChatMessage').length;
                if(total_messages > total_shown_messages)
                {
                    conversationWindow.find('#show_older').show();
                } else {
                    conversationWindow.find('#show_older').hide();
                }

                // Mark as read all messages of current user for the conversation
                markMessagesAsRead(conversation_id);
            },
            error: function(err){
                console.log('Error retrieving messages for conversation ' + conversation_id);
                console.log(err.message);
            }
        });
    } else {
        // Mark as read all messages of current user for the conversation
        markMessagesAsRead(conversation_id);
    }

    // Check online status
    if(conversationItem.attr('data-user'))
    {
        XLC.socket.emit('check_user_status', XLC.user, [conversationItem.attr('data-user')]);
    } else if(conversationItem.attr('data-group')) {

    }

    let onConversationWindowLoad = new CustomEvent('onConversationWindowLoad', {
        'detail': {
            'user_id': conversationItem.attr('data-user'),
            'conversation_id': conversation_id
        }
    });
    document.dispatchEvent(onConversationWindowLoad);
});

// test event listener
/*document.addEventListener('myEventXXX', function(e){
    console.log('Test event "myEventXXX" data:', e.detail);
}, false);*/

// For idle status
/*$(document).mousemove(function(e){
    if(XLChat.user.status == 'idle')
    {
        XLChat.socket.emit('status online', XLChat.user);
    }
});*/

/*$.fn.isInViewPort = function(){
    return $(this).is(':in-viewport');
};*/

// Show older messages
$('body').on('click', '.gqWhisperContainer #show_older', function(){
    var conversation_id = $(this).closest('.gqWhisperContainer').attr('data-conversation');

    $.ajax({
        url: Routing.generate('xlabs_chat_get_conversation_messages'),
        data: {
            conversation_id: conversation_id,
            last_message_id: $(this).parent().find('.gqChatMessage:first').attr('data-message')
        },
        type: 'post',
        dataType: 'json',
        success: function(messages){
            var total_messages = messages.total_messages;
            $.each(messages.messages, function(i, message){
                prependMessageToConversationWindow(conversation_id, message);
            });

            var conversationWindow = getConversationWindow(conversation_id);
            var total_shown_messages = conversationWindow.find('.gqChatMessage').length;
            if(total_messages > total_shown_messages)
            {
                conversationWindow.find('#show_older').show();
            } else {
                conversationWindow.find('#show_older').hide();
            }
        },
        error: function(err){
            console.log('Error retrieving messages for conversation ' + conversation_id);
            console.log(err.message);
        }
    });
});

// Send message from inside conversation
$('body').on('submit', '#chatContainer #submitMessage', function(){
    var body = $(this).find('input.gqInputMessage').val();
    if(body != '')
    {
        var XLC_message = new XLC.message();
        var conversation_id = $(this).closest('.gqWhisperContainer').attr('data-conversation');
        XLC_message.conversation_id = conversation_id;
        XLC_message.sender_id = XLC.user.id;
        XLC_message.participants = $(this).closest('.gqWhisperContainer').attr('data-participants');
        XLC_message.body = urlify(body);
        XLC_message.timestamp = Math.floor(Date.now() / 1000);

        // Cleanup script tags
        var re = /<script\b[^>]*>([\s\S]*?)<\/script>/gm;
        var match;
        while(match = re.exec(XLC_message.body)) {
            // full match is in match[0], whereas captured groups are in ...[1], ...[2], etc.
            XLC_message.body = XLC_message.body.replace(match[0], '');
        }

        if(XLC_message.body != '')
        {
            XLC.socket.emit('message', XLC_message);
        }

        $('#chatContainer #inputMessage').val('');

        var participants = XLC_message.participants.split(',').filter(function(participant){
            return (participant != XLC.user.id);
        });
        for(var i in participants)
        {
            XLC.socket.emit('stopTyping', conversation_id, participants[i]);
        }

        XLC.settings.idle_counter = 0;
        return false;
    } else {
        return false;
    }
});
$('body').on('click', '.gqSubmit', function(){
    $('#chatContainer #submitMessage').submit();
});

// Typing status
$('body').on('keydown', '#chatContainer #inputMessage', function(){
    var conversation_id = $(this).closest('.gqWhisperContainer').attr('data-conversation');
    var conversationItem = getConversationItem(conversation_id);
    var i, participants;

    clearTimeout(XLC.settings.typing_timer);

    if($(this).val() != '')
    {
        if(!XLC.typing_status_checked)
        {
            XLC.typing_status_checked = true;
            participants = conversationItem.attr('data-participants').split(',').filter(function(participant){
                return (participant != XLC.user.id);
            });
            for(i in participants)
            {
                XLC.socket.emit('isTyping', conversation_id, participants[i]);
            }
        }
    }
}).on('keyup', '#chatContainer #inputMessage', function(){
    var conversation_id = $(this).closest('.gqWhisperContainer').attr('data-conversation');
    var conversationItem = getConversationItem(conversation_id);
    var i, participants;

    clearTimeout(XLC.settings.typing_timer);
    XLC.settings.typing_timer = setTimeout(function(){
        participants = conversationItem.attr('data-participants').split(',').filter(function(participant){
            return (participant != XLC.user.id);
        });
        for(i in participants)
        {
            XLC.socket.emit('stopTyping', conversation_id, participants[i]);
        }
        XLC.typing_status_checked = false;
    }, XLC.settings.typing_time);
});

// Send image
$('body').on('click', '.gqUserActions #send_image', function(){
    var inputFile = $(this).closest('.gqUserActions').find('input#send_image_input[type="file"]');
    inputFile.click();
});
// Handle upload
$('body').on('change', '.gqUserActions input[type="file"]', function(e){
    var conversationWindow = $(this).closest('.gqWhisperContainer');
    var messages_container = conversationWindow.find('#messagesGoHere');
    var files = e.target.files;
    var data = new FormData();
    let max_filesize_exceeded = false;
    $.each(files, function(key, value)
    {
        if(value.size > XLC.settings.max_file_size)
        {
            //showErrorInConversationWindow(conversationWindow, 'Uploaded file size too big: File can´t be bigger than ' + parseFloat(parseInt(XLC.settings.max_file_size) / 1024 / 1024).toFixed(2) + 'Mb');
            showErrorInConversationWindow(conversationWindow, 'Uploaded file size too big: File can´t be bigger than ' + formatBytes(XLC.settings.max_file_size));

            /*showNotification({
                //destinatary_id: XLC.user.id,
                title: 'Uploaded file size too big',
                msg: 'File can´t be bigger than ' + formatBytes(XLC.settings.max_file_size),
                icon: false,
                muted: false,
                link: false,
                sound: false,
                fadeout_after_ms: 4000
                //onClick: ''
            });*/
            max_filesize_exceeded = true;
            return false;
        }
        data.append(key, value);
    });

    if(max_filesize_exceeded)
    {
        return false;
    }

    conversationWindow.find('.gqUploadProgress').css('width', '0%').show();
    $.ajax({
        url: Routing.generate('xlabs_chat_upload_image'),
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        xhr: function () {
            let xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress', function (evt) {
                if(evt.lengthComputable)
                {
                    let percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    conversationWindow.find('.gqUploadProgress').css('width', percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data, textStatus, jqXHR) {
            conversationWindow.find('.gqUploadProgress').hide();
            if(typeof data.error === 'undefined')
            {
                // success
                var msg = [];
                $.each(data.files, function(i, v){
                    if(XLC.settings.images_prefix)
                    {
                        v = XLC.settings.images_prefix + v;
                    } else {
                        v = '<a class="xlc_link" target="_blank" href="/' + v + '"><img src="/' + v +'" /></a>';
                    }
                    msg.push(v);
                });
                if(msg.length)
                {
                    conversationWindow.find('#inputMessage').val(msg.join('<br />')).prop('value', msg.join('<br />'));
                    conversationWindow.find('#submitMessage').submit();
                }
            } else {
                showErrorInConversationWindow(conversationWindow, 'Error trying to send image: ' +  data.error);
                /*showNotification({
                    title: 'Error trying to send image',
                    msg: data.error,
                    icon: false,
                    muted: false,
                    link: false,
                    sound: false,
                    fadeout_after_ms: 4000
                    //onClick: ''
                });*/
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            conversationWindow.find('.gqUploadProgress').hide();
            showErrorInConversationWindow(conversationWindow, 'Error trying to send image: ' +  textStatus);
            /*showNotification({
                //destinatary_id: XLC.user.id,
                title: 'Error trying to send image',
                msg: textStatus,
                icon: false,
                muted: false,
                link: false,
                sound: false,
                fadeout_after_ms: 4000
                //onClick: ''
            });*/
        }
    });
});

// Block/Unblock user
$('body').on('click', '.gqWhisperContainer #block_user, .gqWhisperContainer #unblock_user', function(){
    var but = $(this);
    var conversationWindow = $(this).closest('.gqWhisperContainer');
    var messages_container = conversationWindow.find('#messagesGoHere');

    $.ajax({
        url: Routing.generate('xlabs_chat_block_user'),
        type: 'POST',
        data: {
            blocked_uid: but.attr('data-user'),
        },
        cache: false,
        dataType: 'json',
        success: function(data, textStatus, jqXHR) {
            if(typeof data.error === 'undefined')
            {
                if(data.status == 0)
                {
                    conversationWindow.find('#block_user').show();
                    conversationWindow.find('#unblock_user').hide();
                    var index = XLC.blocked.indexOf(but.attr('data-user'));
                    if(index > -1)
                    {
                        XLC.blocked.splice(index, 1);
                    }
                } else if(data.status == 1) {
                    conversationWindow.find('#block_user').hide();
                    conversationWindow.find('#unblock_user').show();
                    XLC.blocked.push(but.attr('data-user'));
                }
            } else {
                showErrorInConversationWindow(conversationWindow, data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showErrorInConversationWindow(conversationWindow, 'Request could not be performed. Try again in a few moments.');
        }
    });
});

// Report user
$('body').on('click', '.gqWhisperContainer #report_user', function(){
    var but = $(this);
    var conversationWindow = $(this).closest('.gqWhisperContainer');
    var messages_container = conversationWindow.find('#messagesGoHere');
    var conversation_id = conversationWindow.attr('data-conversation');
    var conversationItem = getConversationItem(conversation_id);

    $.ajax({
        url: Routing.generate('xlabs_chat_report_conversation'),
        type: 'POST',
        data: {
            conversation_id: conversation_id
        },
        cache: false,
        dataType: 'json',
        success: function(data, textStatus, jqXHR) {
            if(typeof data.error === 'undefined')
            {
                if(XLC.blocked.indexOf(parseInt(conversationItem.attr('data-user'))) == -1)
                {
                    if(confirm('Do you also want to block "' + XLC.users[conversationItem.attr('data-user')].username + '"?'))
                    {
                        conversationWindow.find('#block_user').click();
                    }
                }
            } else {
                showErrorInConversationWindow(conversationWindow, data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showErrorInConversationWindow(conversationWindow, 'Report could not be sent at this time. Try again in a few moments.');
        }
    });
});