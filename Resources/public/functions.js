function sortParticipants(aUsers)
{
    return aUsers.sort(function(a, b){return a-b});
}

function getConversationId(aUsers)
{
    return md5(sortParticipants(aUsers).join(','));
}

// Search for urls in text and convert them into links
function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    var preview;
    return text.replace(urlRegex, function(url) {
        var file_ext = url.substr(url.length - 4);
        if(['.jpg', 'jpeg', '.png', '.bmp', '.gif'].indexOf(file_ext.toLowerCase()) > -1)
        {
            return '<a class="xlc_link" target="_blank" href="' + url + '"><img src="' + url +'" /></a>';
        }

        // Try curl request on link to show preview
        /*getLinkPreview(url).then(function(data){
            //console.log(data.response);
            preview = data.response;
        }).catch(function(err){
            console.log('Error getting link preview: ', err);
        });*/
        return '<a class="xlc_link" target="_blank" href="' + url + '">' + url + '</a>';
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

function getLinkPreview(url)
{
    return new Promise(function(resolve, reject) {
        $.ajax({
            url: 'http://playground.ajaxtown.com/link_preview/class.linkpreview.php?url=' + url,
            //url: 'http://api.linkpreview.net',
            //url: url,
            type: 'GET',
            //processData: true,
            //dataType: 'jsonp',
            //data: {q: url, key: 123456},
            //async: false,
            //headers: {
            //    'x-my-custom-header': 'some value'
            //},
            beforeSend: function(xhr){
                //xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            },
            success: function(data){
                resolve({
                    response: data.responseText
                });
                //console.log(data.responseText);
                //var matches = data.responseText.match(/<title>(.*?)<\/title>/);
                //console.log(matches);
                //alert(matches[0]);
            },
            error: function(e){
                reject(e);
            }
        });
    });
}

// Strip script tags
/*function stripScripts(s) {
    var div = document.createElement('div');
    div.innerHTML = s;
    var scripts = div.getElementsByTagName('script');
    var i = scripts.length;
    while (i--) {
        scripts[i].parentNode.removeChild(scripts[i]);
    }
    return div.innerHTML;
}*/

function getUserItem(user_id)
{
    var userItem = $('#userList li[data-user="' + user_id + '"]');
    return (userItem.length ? userItem : false);
}

function getOnlineUserItem(user_id)
{
    var onlineUserItem = $('#onlineUsersList li[data-user="' + user_id + '"]');
    return (onlineUserItem.length ? onlineUserItem : false);
}

function isOnlineUsersListEmpty()
{
    return !$('#onlineUsersList li[data-user]').length;
}

function createOnlineUserItem(user_id, prepend_in_list = false)
{
    var onlineUserItem = getOnlineUserItem(user_id);
    if(!onlineUserItem)
    {
        var html = '';
        html += '<li id="" class="gqOnlineUser" data-user="' + XLC.users[user_id][XLC.user_entity_mappings.id] + '" data-conversation="">\n';
        if(XLC.users[user_id].avatar)
        {
            html += '<img class="gqUserIcon" src="' +  XLC.users[user_id].avatar + '" />\n';
        } else {
            html += '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
                '         width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve" class="gqUserIcon">\n' +
                '        <path fill="#939393" d="M39.992,33.117c-0.878-5.146-2.269-8.38-6.583-9.83c1.573-1.809,2.518-4.139,2.518-6.678\n' +
                '            c0-5.778-4.902-10.48-10.927-10.48c-6.025,0-10.928,4.702-10.928,10.48c0,2.536,0.946,4.866,2.517,6.68\n' +
                '            c-4.314,1.448-5.706,4.687-6.582,9.828c-0.613,3.59-0.104,6.328,1.517,8.165c1.521,1.717,3.978,2.588,7.301,2.588h12.349\n' +
                '            c3.322,0,5.78-0.871,7.298-2.588C40.096,39.455,40.608,36.707,39.992,33.117z M25,8.007c4.946,0,8.969,3.858,8.969,8.603\n' +
                '            c0,4.742-4.022,8.603-8.969,8.603c-4.946,0-8.971-3.86-8.971-8.603C16.029,11.865,20.054,8.007,25,8.007z M36.982,40.071\n' +
                '            c-1.129,1.272-3.085,1.923-5.809,1.923H18.827c-2.726,0-4.681-0.65-5.811-1.923c-1.232-1.392-1.594-3.632-1.079-6.649\n' +
                '            c0.951-5.579,2.487-7.717,6.277-8.615c1.865,1.423,4.222,2.282,6.786,2.282c2.565,0,4.92-0.859,6.785-2.286\n' +
                '            c3.792,0.898,5.321,3.033,6.277,8.616C38.578,36.445,38.215,38.68,36.982,40.071z">\n' +
                '        </path>\n' +
                '    </svg>\n';
        }
        html += '    <div class="gqStatus">\n' +
            '        <span class="gqOnline" style="display: block;"></span>\n' +
            '        <span class="gqOffline" style="display: none;"></span>\n' +
            '    </div>';
        html += '        <div class="gqUserMeta"><span class="gqUserTitle">' + XLC.users[user_id][XLC.user_entity_mappings.username] + '</span><div class="gqBorder"></div></div>\n' +
            '        <!--(<span class="gqUserUnreadMessages"><span>0</span></span> new messages)\n' +
            '        - <span class="gqUserStatus" style="font-style: italic;"></span>\n' +
            '        <p></p>-->\n' +
            '    </li>';

        if(prepend_in_list)
        {
            $('#onlineUsersList').prepend($(html));
        } else {
            $('#onlineUsersList').append($(html));
        }
    }
}

function getConversationItem(conversation_id)
{
    var conversationItem = $('#conversationList li[data-conversation="' + conversation_id + '"]');
    return (conversationItem.length ? conversationItem : false);
}

function getConversationWindow(conversation_id)
{
    var conversationWindow = $('#chatContainer [data-conversation="' + conversation_id + '"]');
    return (conversationWindow.length ? conversationWindow : false);
}

function createConversationItem(XLC_conversation, prepend_in_list = false)
{
    var conversationItem = getConversationItem(XLC_conversation.conversation_id);
    if(!conversationItem)
    {
        conversationItem = $('#conversationList li.sample').clone();
        conversationItem.attr({
            'data-conversation': XLC_conversation.conversation_id,
            'data-participants': XLC_conversation.participants.join(',')
        }).removeClass('sample');
        conversationItem.find('.gqUserTitle').text(XLC_conversation.title);
        conversationItem.find('.gqUserUnreadMessages').hide().find('span').text(XLC_conversation.unread_messages);
        if(XLC_conversation.unread_messages > 0)
        {
            conversationItem.find('.gqUserUnreadMessages').show();
        }
        //conversationItem.find('img').attr('src', '');

        switch(XLC_conversation.conversation_type)
        {
            case 'user':
                var destinatary_id = XLC_conversation.participants.filter(function(participant){
                    return (participant != XLC.user.id);
                })[0];
                conversationItem.attr('data-user', destinatary_id);

                if(destinatary_id in XLC.users && XLC.users[destinatary_id].avatar)
                {
                    conversationItem.find('img.gqUserIcon').attr('src', XLC.users[destinatary_id].avatar);
                    conversationItem.find('svg.gqUserIcon').remove();
                } else {
                    conversationItem.find('img.gqUserIcon').remove();
                }

                XLC.socket.emit('check_user_status', XLC.user, [destinatary_id]);
                break;
            case 'group':
                conversationItem.attr('data-group', '<set_group_id_here>');

                if(XLC_conversation.avatar)
                {
                    conversationItem.find('img.gqUserIcon').attr('src', XLC_conversation.avatar);
                    conversationItem.find('svg.gqUserIcon').remove();
                } else {
                    conversationItem.find('img.gqUserIcon').remove();
                }
                break;
        }

        if(prepend_in_list)
        {
            $('#conversationList').prepend(conversationItem);
        } else {
            $('#conversationList').append(conversationItem);
        }

        // Load last message
        if(XLC_conversation.last_message)
        {
            if(XLC_conversation.last_message.indexOf('<a ') > -1)
            {
                if(XLC_conversation.last_message.indexOf('<img ') > -1)
                {
                    conversationItem.find('.gqLastMessage.gqImage').attr('data-last-visible', true).show();
                    conversationItem.find('.gqLastMessage.gqLink').removeAttr('data-last-visible').hide();
                    conversationItem.find('.gqLastMessage.gqText').removeAttr('data-last-visible').hide();
                    conversationItem.find('.gqUserTyping').hide();
                } else {
                    conversationItem.find('.gqLastMessage.gqLink').attr('data-last-visible', true).show();
                    conversationItem.find('.gqLastMessage.gqImage').removeAttr('data-last-visible').hide();
                    conversationItem.find('.gqLastMessage.gqText').removeAttr('data-last-visible').hide();
                    conversationItem.find('.gqUserTyping').hide();
                }
            } else {
                conversationItem.find('.gqLastMessage.gqText').attr('data-last-visible', true).text(XLC_conversation.last_message).show();
                conversationItem.find('.gqLastMessage.gqImage').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqLastMessage.gqLink').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqUserTyping').hide();
            }
        }

        return true;
    }

    // Load last message
    if(XLC_conversation.last_message)
    {
        if(XLC_conversation.last_message.indexOf('<a ') > -1)
        {
            if(XLC_conversation.last_message.indexOf('<img ') > -1)
            {
                conversationItem.find('.gqLastMessage.gqImage').attr('data-last-visible', true).show();
                conversationItem.find('.gqLastMessage.gqLink').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqLastMessage.gqText').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqUserTyping').hide();
            } else {
                conversationItem.find('.gqLastMessage.gqLink').attr('data-last-visible', true).show();
                conversationItem.find('.gqLastMessage.gqImage').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqLastMessage.gqText').removeAttr('data-last-visible').hide();
                conversationItem.find('.gqUserTyping').hide();
            }
        } else {
            conversationItem.find('.gqLastMessage.gqText').attr('data-last-visible', true).text(XLC_conversation.last_message).show();
            conversationItem.find('.gqLastMessage.gqImage').removeAttr('data-last-visible').hide();
            conversationItem.find('.gqLastMessage.gqLink').removeAttr('data-last-visible').hide();
            conversationItem.find('.gqUserTyping').hide();
        }
    }

    return false;
}

// Create conversation if doesn´t exist
function createConversationWindow(XLC_conversation)
{
    var conversation_id = XLC_conversation.conversation_id;
    var conversationItem = getConversationItem(conversation_id);
    var conversationWindow = getConversationWindow(conversation_id);
    if(!conversationWindow)
    {
        conversationWindow = $('#chatContainer div.gqWhisperContainer.sample').clone();
        conversationWindow.attr({
            'data-conversation': conversation_id,
            'data-participants': XLC_conversation.participants,
        }).removeClass('sample');

        // Avatar
        if(conversationItem.attr('data-user') in XLC.users && XLC.users[conversationItem.attr('data-user')].avatar)
        {
            conversationWindow.find('img.gqUserAvatar').attr('src', XLC.users[conversationItem.attr('data-user')].avatar);
            conversationWindow.find('svg.gqUserAvatar').remove();
        } else {
            conversationWindow.find('img.gqUserAvatar').remove();
        }
        conversationWindow.find('.gqUsername').text(conversationItem.find('.gqUserTitle').text());

        // Report button
        conversationWindow.find('#report_user').attr('data-user', conversationItem.attr('data-user'));

        // Block button status
        if(XLC.blocked.indexOf(parseInt(conversationItem.attr('data-user'))) > -1)
        {
            conversationWindow.find('#block_user').attr('data-user', conversationItem.attr('data-user')).hide();
            conversationWindow.find('#unblock_user').attr('data-user', conversationItem.attr('data-user')).show();
        } else {
            conversationWindow.find('#unblock_user').attr('data-user', conversationItem.attr('data-user')).hide();
            conversationWindow.find('#block_user').attr('data-user', conversationItem.attr('data-user')).show();
        }

        conversationWindow.find('#messagesGoHere').empty().append($('<button id="show_older" class="gqShowMore">Show older</button>').addClass('gqShowMore').hide()).append($('<a name="bottom"></a>'));
        $('#chatContainer').append(conversationWindow);
    }
    return conversationWindow;
}

// Show conversation
function activateConversationWindow(XLC_conversation)
{
    var conversationWindow = createConversationWindow(XLC_conversation);
    conversationWindow.addClass('active');
    conversationWindow.find('#status' ).addClass('active');
}

// Add message to conversation
function appendMessageToConversationWindow(conversation_id, message)
{
    let conversationWindow = getConversationWindow(conversation_id);
    let messages_container = conversationWindow.find('#messagesGoHere');

    let msg = $('#chatContainer div.sample #messagesGoHere .gqChatMessage.standard.sample').clone();
    msg.attr({
        'data-author-id': message.author.id,
        'data-message': message.id ? message.id : ''
    }).removeClass('standard').removeClass('sample');
    if(message.author.id == XLC.user.id)
    {
        msg.find('.gqChatUsername').text('Me');
        msg.addClass('gqRight');
    } else {
        msg.find('.gqChatUsername').text(message.author.username);
        msg.addClass('gqLeft');
    }
    let creationdate = new Date(message.creationdate*1000);
    creationdate = creationdate.getHours() + ':' + ("0" + creationdate.getMinutes()).substr(-2);
    msg.find('.gqChatTime').text(creationdate);

    // Image preloader
    if(message.body.indexOf('<img ') > -1 && $(message.body).find('img').length)
    {
        let preloader = $('#chatContainer div.gqImagePreloader.sample').clone();
        preloader.removeClass('sample');
        msg.find('.gqChatContent').append(preloader.show());

        let img = new Image();
        let imgW, imgH;
        img.onload = function() {
            imgW = this.width;
            imgH = this.height;

            msg.find('.gqChatContent').html(message.body);
            preloader = null;
            $(messages_container).get(0).scrollTop = $(messages_container).get(0).scrollHeight;
            /*$(messages_container).animate({
                scrollTop: $(messages_container).find('a[name="bottom"]').offset().top
            }, 50);*/
        };
        img.onerror = function(e) {
            let broken_img = $('#chatContainer div.gqImageError.sample').clone();
            broken_img.removeClass('sample');
            msg.find('.gqChatContent').html('').append(broken_img.show());
            preloader = null;
            $(messages_container).get(0).scrollTop = $(messages_container).get(0).scrollHeight;
            /*$(messages_container).animate({
                scrollTop: $(messages_container).find('a[name="bottom"]').offset().top
            }, 50);*/
        };
        $(messages_container).find('a[name="bottom"]').before(msg);
        img.src = $(message.body).find('img').attr('src');
    } else {
        msg.find('.gqChatContent').html(message.body);
        $(messages_container).find('a[name="bottom"]').before(msg);
        $(messages_container).get(0).scrollTop = $(messages_container).get(0).scrollHeight;
        /*$(messages_container).animate({
            scrollTop: $(messages_container).find('a[name="bottom"]').offset().top
        }, 50);*/
    }

    // Append message and scroll down
    /*$(messages_container).find('a[name="bottom"]').before(msg);
    $(messages_container).animate({
        scrollTop: $(messages_container).find('a[name="bottom"]').offset().top
    }, 50);*/

    // Update unread messages counter if necessary
    //if(!conversationWindow.isInViewPort() || !conversationWindow.is(':visible'))
    if(conversationWindow.is(':visible'))
    {
        // Mark as read all messages of current user for the conversation
        //markMessagesAsRead(conversation_id);
    } else {
        if(XLC.user.id != parseInt(message.author.id))
        {
            updateUnreadMessagesCounter(conversation_id);
        }
    }
}

// Prepend message to conversation (show older)
function prependMessageToConversationWindow(conversation_id, message)
{
    var conversationWindow = getConversationWindow(conversation_id);
    var messages_container = conversationWindow.find('#messagesGoHere');

    var msg = $('#chatContainer div.sample #messagesGoHere .gqChatMessage.standard.sample').clone();
    msg.attr({
        'data-author-id': message.author.id,
        'data-message': message.id ? message.id : ''
    }).removeClass('standard').removeClass('sample');
    if(message.author.id == XLC.user.id)
    {
        msg.find('.gqChatUsername').text('Me');
        msg.addClass('gqRight');
    } else {
        msg.find('.gqChatUsername').text(message.author.username);
        msg.addClass('gqLeft');
    }
    var creationdate = new Date(message.creationdate*1000);
    creationdate = creationdate.getHours() + ':' + ("0" + creationdate.getMinutes()).substr(-2);
    msg.find('.gqChatTime').text(creationdate);
    //msg.find('.gqChatContent').html(message.body);

    // Image preloader
    if(message.body.indexOf('<img ') > -1 && $(message.body).find('img').length)
    {
        let preloader = $('#chatContainer div.gqImagePreloader.sample').clone();
        preloader.removeClass('sample');
        msg.find('.gqChatContent').append(preloader.show());

        let img = new Image();
        let imgW, imgH;
        img.onload = function() {
            imgW = this.width;
            imgH = this.height;

            msg.find('.gqChatContent').html(message.body);
            preloader = null;
        };
        img.onerror = function(e) {
            let broken_img = $('#chatContainer div.gqImageError.sample').clone();
            broken_img.removeClass('sample');
            msg.find('.gqChatContent').html('').append(broken_img.show());
        };
        $(messages_container).find('.gqChatMessage:first').before(msg);
        img.src = $(message.body).find('img').attr('src');
    } else {
        msg.find('.gqChatContent').html(message.body);
        $(messages_container).find('.gqChatMessage:first').before(msg);
    }

    //$(messages_container).find('.gqChatMessage:first').before(msg);
}

function updateUnreadMessagesCounter(conversation_id)
{
    let conversationItem = getConversationItem(conversation_id);
    let currentUnreadMessages_container = conversationItem.find('span.gqUserUnreadMessages');
    let currentUnreadMessages = currentUnreadMessages_container.find('span').text();
    currentUnreadMessages_container.show().find('span').text(parseInt(currentUnreadMessages) + 1);

    // blink or play sound here
    conversationItem.addClass('incoming_msg');
    setTimeout(function(){
        conversationItem.removeClass('incoming_msg');
    }, 2000);
}

// Mark conversation messages as read for a user
function markMessagesAsRead(conversation_id)
{
    let conversationItem = getConversationItem(conversation_id);
    let currentUnreadMessages_container = conversationItem.find('span.gqUserUnreadMessages');
    let currentUnreadMessages = currentUnreadMessages_container.find('span').text();

    if(currentUnreadMessages != '' && parseInt(currentUnreadMessages) > 0)
    {
        $.ajax({
            url: Routing.generate('xlabs_chat_conversation_messages_mark_as_read'),
            data: {
                conversation_id: conversation_id
            },
            type: 'post',
            dataType: 'html',
            success: function(total_messages_marked){
                XLC.socket.emit('mark_conversation_as_read', conversation_id, XLC.user, total_messages_marked);
            },
            error: function(err){
                console.log('Error marking messages as read for conversation ' + conversation_id);
                console.log(err.message);
            }
        });
    }
}

function showErrorInConversationWindow(conversationWindow, description, sticky = false)
{
    let messages_container = conversationWindow.find('#messagesGoHere');
    if(messages_container.length)
    {
        messages_container.append($('<span class="gqError">' + description + '</span>'));
        $(messages_container).animate({
            scrollTop: $(messages_container).find('a[name="bottom"]').offset().top
        }, 50);
        if(!sticky)
        {
            setTimeout(function(){
                if(messages_container.find('.gqError').length) {
                    messages_container.find('.gqError').remove();
                }
            }, 3000);
        }
    }
}

// Speech to text
/*var XLC_speech_recognition = {};
try {
    XLC_speech_recognition.SpeechRecognition = window.SpeechRecognition || window.mozSpeechRecognition || window.webkitSpeechRecognition;
    XLC_speech_recognition.recognition = new XLC_speech_recognition.SpeechRecognition();
}
catch(e) {
    console.error(e);
    $(document).ready(function(){
        $('#speech_recognition').remove();
    });
}*/
$(document).ready(function(){
    $('#speech_recognition').remove();
    $('#emoji_select').remove();
});

// HTML5 Notifications
var XLC_notifications = {};
XLC_notifications.Notification = window.Notification || window.mozNotification || window.webkitNotification;
XLC_notifications.delayTimeout = 0;
XLC_notifications.Notification.requestPermission(function(permission) {
    // console.log(permission);
});

const PERMISSION_DEFAULT = "default",
    PERMISSION_GRANTED = "granted",
    PERMISSION_DENIED = "denied",
    PERMISSION = [PERMISSION_GRANTED, PERMISSION_DEFAULT, PERMISSION_DENIED];

function checkPermission()
{
    var permission = PERMISSION_DENIED;
    if(window.webkitNotifications && window.webkitNotifications.checkPermission){
        permission = PERMISSION[window.webkitNotifications.checkPermission()];
    } else {
        if (navigator.mozNotification) {
            permission = PERMISSION_GRANTED;
        } else {
            if (window.Notification && window.Notification.permission) {
                permission = window.Notification.permission;
            }
        }
    }
    return permission;
}

function showNotification(data)
{
    if(checkPermission() === PERMISSION_GRANTED)
    {
        window.setTimeout(function(){
            var instance = new XLC_notifications.Notification(
                data.title ? data.title : "Chat Notification", {
                    body: '' + data.msg,
                    dir: 'auto',
                    //tag: 'random_id_',
                    icon: (data.icon && data.icon != '') ? data.icon : "{{ app.request.getUriForPath('') ~ asset('/bundles/xlabsnotify/xlabs.png') }}"
                }
            );

            instance.onclick = function() {
                if(data.link)
                {
                    var new_tab = window.open(data.link, '_blank');
                    new_tab.focus();
                }
                /*if(data.onClick !== undefined && window[data.onClick] === "function")
                {
                    var onClick = data.onClick;
                    if('destinatary_id' in data) delete data['destinatary_id'];
                    if('onClick' in data) delete data['onClick'];
                    window[onClick](data);
                }*/
            };
            instance.onerror = function() {
                // Something to do
            };
            instance.onshow = function() {
                // Notification sound
                if(!data.muted)
                {
                    playNotificationSound(data.sound ? data.sound : '/bundles/xlabschat/sounds/arpeggio.mp3');
                }
            };
            instance.onclose = function() {
                // Something to do
            };
            if(data.fadeout_after_ms)
            {
                setTimeout(instance.close.bind(instance), data.fadeout_after_ms);
            }
        }, XLC_notifications.delayTimeout);
    }
    return false;
}

function playNotificationSound(file)
{
    var player = $('audio#xlabs_chat_player');
    if(player.length)
    {
        if(player.find('source#mp3').attr('src') != file)
        {
            player.find('source#mp3').attr('src', file);
            //player.find('source#ogg').attr('src', notification_sound[notification_sound_key].ogg);
            player.find('embed').attr('src', file);
            player[0].load();
        }
        player[0].play();
    }
}

/* Speech API - Speak stuff */
function speakText(text)
{
    //console.log(voices); // check available voices
    var synth = window.speechSynthesis;
    if(typeof synth === 'undefined') {
        return;
    }
    var voices = synth.getVoices();
    var utterThis = new SpeechSynthesisUtterance(text);
    for(i = 0; i < voices.length ; i++) {
        //if(voices[i].lang === 'es-ES') {
        if(voices[i].lang === 'en-US') {
            utterThis.voice = voices[i];
        }
    }
    utterThis.pitch = 1.2; // value between 0-2
    utterThis.rate = 1; // value between 0-2
    synth.speak(utterThis);
}

function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}