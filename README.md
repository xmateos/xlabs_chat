A redis driven like engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/chatbundle
```

This bundle depends on "xlabs/rabbitmqbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\ChatBundle\XLabsChatBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_chat:
    resource: .
    type: xlabs_chat_routing
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_rabbit_mq:
    ...
  
x_labs_chat:
    user_entity: YourBundle\Entity\YourFOSUserExtendedEntity
    user_entity_mappings:
        id: <your_user_entity_id_fieldname>
        username: <your_user_entity_usernam_fieldname>
        avatar: <your_user_entity_avatar_fieldname>
    url: /chat
    nodejs_settings:
        host: your.host.com
        port: 3026
        schema: https
        ssl_key: /etc/nginx/.../your.host.com.key
        ssl_cert: /etc/nginx/.../your.host.com.crt
        ssl_bundle: /etc/nginx/.../your.host.com.(bundle | crt)
    redis_settings:
        host: 192.168.5.23
        port: 6379
        database_id: 20
        _key_namespace: 'your:namespace:chat'
    rabbitmq_settings:
        queue_prefix: 'your_prefix' (queue will be named by this prefix followed by "_chat"); needs to match the one set in x_labs_rabbit_mq bundle config
    uploads:
        folder: your/upload/folder/inside/web/folder
        allowed_extensions: ['jpg', 'jpeg']
        max_file_size: 1048576 (in bytes)
    settings:
        message_ttl: <expiration_period_in_hours> | false
        images_prefix: 'https://cdn.static.stiffia.com/' (for avatar and uploaded images)
        report_to: ['xavi.mateos@manicamedia.com']
```

Also, if you have 'resolve_target_entities' set in doctrine´s orm config section, you will need to add the following:
``` yml
# app/config/config.yml

doctrine:
    ...
    orm:
        ...
        resolve_target_entities:
            ...
            XLabs\ChatBundle\Model\XLabsChatUserInterface: YourBundle\Entity\YourFOSUserExtendedEntity
```

Make sure you update all assets:
```bash
php app/console assets:install --symlink
```

Run command to create NodeJS server file:
```bash
php app/console xlabs_chat:create:server
```

Install NodeJS dependencies under "web/chat/":
```bash
npm install
```

### Extra functionalities ###
There´s a service included in the bundle which will return user_ids based on some cryteria. Have a look at 'xlabs_chat_user_manager' service and its method 'searchUsers', for instance, if you want to list the current active chat users.

### Events ###
In order for the chat to know about online/offline users, make sure to create login/logout listeners, and add the following lines:
```php
namespace YourBundle\LoginEventListener;

use XLabs\ChatBundle\Event\XLabsChatUserOnline;

class MyLoginListener extends Event
{
    public function yourCustomMethod(Event $event)
    {
        ...
    
        $event = new XLabsChatUserOnline(array(
            'user_id' => $user->getId()
        ));
        $this->container->get('event_dispatcher')->dispatch(XLabsChatUserOnline::NAME, $event);
    }
}
```
```php
namespace YourBundle\LogoutEventListener;

use XLabs\ChatBundle\Event\XLabsChatUserOffline;

class MyLogoutListener extends Event
{
    public function yourCustomMethod(Event $event)
    {
        ...
    
        $event = new XLabsChatUserOffline(array(
            'user_id' => $user->getId()
        ));
        $this->container->get('event_dispatcher')->dispatch(XLabsChatUserOffline::NAME, $event);
    }
}
```

### Usage ###
Launch the NodeJS server:
```bash
node web/chat/server.js
```
Launch RabbitMQ consumer that stores messages in DB:
```bash
php app/console xlabs_chat:message:store
```
Add the following command in your crontab in order to delete expired messages:
```bash
0 1 * * * php app/console xlabs_chat:messages:expire
```

You can add the following right before the <\/body> tag of you parent template if you want to have a small new message notifier:
{{ render(controller('XLabsChatBundle:Chat:loader')) }}

### Requirements ###
Make sure that doctrine has the following config setting:
```yml
doctrine:
    dbal:
        ...
        charset:  UTF8MB4
```

The node js connection is made through a nginx reverse proxy. Make sure to set a nginx vhost:
```yml
server {
    listen 443 ssl;

    server_name <x_labs_chat.nodejs_settings.host>;

    ## SSL settings
    ssl on;
    ssl_certificate <x_labs_chat.nodejs_settings.ssl_cert>;
    ssl_certificate_key <x_labs_chat.nodejs_settings.ssl_key>;

    ## SSL caching/optimization
    ssl_protocols        SSLv3 TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers RC4:HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;
    keepalive_timeout    60;
    ssl_session_cache    shared:SSL:10m;
    ssl_session_timeout  10m;

	location / {
        #proxy_set_header 'Access-Control-Allow-Origin' '*';

        #proxy_set_header X-Real-IP $remote_addr;
        #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        #proxy_set_header Host $http_host;
        #proxy_set_header X-NginX-Proxy true;
        #proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        proxy_pass https://<your_internal_nodejs_server_ip>:<x_labs_chat.nodejs_settings.port>;
    }
}
```

In order to prevent the server to overload, you can schedule (crontab) a NodeJS chat server instance restart with this script:
```bash
#!/bin/bash

# Restart chat
# kill node instance and screen
ps -ef | grep <path_to_your_app>/web/chat/server.js | grep -v grep | awk '{print $2}' | xargs kill && screen -S <screen_name> -X quit
# run node instance in screen
screen -dmS <screen_name> sh && screen -S <screen_name> -X stuff "node <path_to_your_app>/web/chat/server.js
"

```