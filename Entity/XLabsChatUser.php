<?php

namespace XLabs\ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use XLabs\ChatBundle\Model\XLabsChatUserInterface;

/**
 * @ORM\Entity(repositoryClass="XLabs\ChatBundle\Repository\XLabsChatUserRepository")
 * @ORM\Table(name="xlabs_chat_users")
 */
class XLabsChatUser
{
    const XLABS_CHAT_USER_STATUS_OFFLINE = 0;
    const XLABS_CHAT_USER_STATUS_ONLINE = 1;
    const XLABS_CHAT_USER_STATUS_IDLE = 2;

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="XLabs\ChatBundle\Model\XLabsChatUserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    protected $user;

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    protected $status;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @ORM\Column(name="banned", type="boolean", nullable=false)
     */
    private $banned;

    public function getBanned()
    {
        return $this->banned;
    }

    public function isBanned()
    {
        return $this->getBanned();
    }

    public function setBanned($banned)
    {
        $this->banned = $banned;
    }

    /**
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $last_login;

    public function setLastLogin(\DateTime $date = null)
    {
        $this->last_login = $date;
        return $this;
    }

    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @ORM\ManyToMany(targetEntity="XLabsChatUser")
     * @ORM\JoinTable(name="xlabs_chat_blocked_users",
     *      joinColumns={@ORM\JoinColumn(name="blocked_by_user_id", referencedColumnName="user_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="blocked_user_id", referencedColumnName="user_id")}
     *      )
     */
    protected $blocked_users;

    public function getBlockedUsers()
    {
        return $this->blocked_users;
    }

    public function addBlockedUser($user)
    {
        $this->blocked_users->add($user);
    }

    public function removeBlockedUser($user)
    {
        if($this->blocked_users->contains($user))
        {
            $this->blocked_users->removeElement($user);
        }
    }

    public function __construct()
    {
        $this->banned = false;
        $this->blocked_users = new ArrayCollection();
    }
}