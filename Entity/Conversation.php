<?php

namespace XLabs\ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;
use XLabs\ChatBundle\Entity\Message;
use XLabs\ChatBundle\Model\XLabsChatUserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_chat_conversations")
 */
class Conversation
{
    const XLABS_CHAT_CONVERSATION_TYPE_USER = 'user';
    const XLABS_CHAT_CONVERSATION_TYPE_GROUP = 'group';
    const RESULT_CACHE_ITEM_PREFIX = 'xlabs_chat_conversation_';
    const RESULT_CACHE_ITEM_TTL = 3600;

    /*
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    /*protected $id;

    public function getId()
    {
        return $this->id;
    }*/

    /**
     * @ORM\Id
     * @ORM\Column(name="conversation_id", type="string", length=128, nullable=false)
     */
    protected $conversation_id;

    public function getConversationId()
    {
        return $this->conversation_id;
    }

    public function setConversationId($conversation_id)
    {
        $this->conversation_id = $conversation_id;
    }

    /**
     * @ORM\Column(name="title", type="string", length=128, nullable=true)
     */
    protected $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    private $creationdate;

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    /**
     * @ORM\Column(name="lastupdate", type="datetime", nullable=false)
     */
    private $lastupdate;

    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    public function setLastupdate($creationdate)
    {
        $this->lastupdate = $creationdate;
    }

    /**
     * @ORM\Column(name="conversation_type", type="string", length=5, nullable=false)
     */
    protected $conversation_type;

    public function getConversationType()
    {
        return $this->conversation_type;
    }

    public function setConversationType($conversation_type)
    {
        $this->conversation_type = $conversation_type;
    }

    /**
     * @ORM\OneToMany(targetEntity="XLabs\ChatBundle\Entity\Message", mappedBy="conversation")
    */
    protected $messages;

    public function getMessages()
    {
        return $this->messages;
    }

    public function addMessage(Message $message)
    {
        $this->messages->add($message);
    }

    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message);
    }

    /*
     * @ORM\OneToMany(targetEntity="ConversationMessage", mappedBy="conversation")
    */
    /*protected $conversationMessages;

    public function getConversationMessages()
    {
        return $this->conversationMessages;
    }

    public function addConversationMessage($conversationMessage)
    {
        $this->conversationMessages->add($conversationMessage);
    }

    public function removeConversationMessage($conversationMessage)
    {
        $this->conversationMessages->removeElement($conversationMessage);
    }*/

    /**
     * @ORM\ManyToMany(targetEntity="XLabs\ChatBundle\Model\XLabsChatUserInterface", orphanRemoval=true)
     * @ORM\JoinTable(name="xlabs_chat_conversations_participants",
     *      joinColumns={@ORM\JoinColumn(name="conversation_id", referencedColumnName="conversation_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="participant_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"id" = "ASC"})
    */
    protected $participants;

    public function getParticipants()
    {
        return $this->participants;
    }

    public function addParticipant($participant)
    {
        $this->participants->add($participant);
    }

    public function removeParticipant($participant)
    {
        $this->participants->removeElement($participant);
    }

    public function __construct()
    {
        $this->conversationMessages = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->creationdate = new DateTime();
    }
}