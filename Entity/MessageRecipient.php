<?php

namespace XLabs\ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;
use XLabs\ChatBundle\Model\XLabsChatUserInterface;
use XLabs\ChatBundle\Entity\Conversation;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 @ORM\Entity(repositoryClass="BF\BikiniBundle\Repository\MessageRepository")
 * @ORM\Table(name="xlabs_chat_message_recipients")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForConversation")
 * })
 */
class MessageRecipient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ChatBundle\Entity\Message", inversedBy="recipients")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id")
     **/
    protected $message;

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ChatBundle\Model\XLabsChatUserInterface")
     * @ORM\JoinColumn(name="destinatary_id", referencedColumnName="id")
     **/
    protected $destinatary;

    public function getDestinatary()
    {
        return $this->destinatary;
    }

    public function setDestinatary($destinatary)
    {
        $this->destinatary = $destinatary;
    }

    /**
     * @ORM\Column(name="read_receipt", type="boolean", nullable=false)
     */
    private $read_receipt;

    public function getRead()
    {
        return $this->read_receipt;
    }

    public function isRead()
    {
        return $this->getRead();
    }

    public function setRead($read_receipt)
    {
        $this->read_receipt = $read_receipt;
    }

    public function __construct()
    {
        $this->read_receipt = false;
    }

    public function getXLabsResultCacheKeyForConversation()
    {
        return $this->getMessage()->getXLabsResultCacheKeyForConversation();
    }
}