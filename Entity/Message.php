<?php

namespace XLabs\ChatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;
use XLabs\ChatBundle\Model\XLabsChatUserInterface;
use XLabs\ChatBundle\Entity\Conversation;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity
 * @ORM\Table(name="xlabs_chat_messages")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForConversation")
 * })
 */
class Message
{
    const RESULT_CACHE_ITEM_PREFIX = 'xlabs_chat_message_';
    const RESULT_CACHE_ITEM_TTL = 3600;

    /*const MESSAGE_TYPE_TEXT = 'text';
    const MESSAGE_TYPE_IMAGE = 'image';
    const MESSAGE_TYPE_LINK = 'link';*/

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    protected $subject;

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @ORM\Column(name="body", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $body;

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    /*
     * @ORM\ManyToOne(targetEntity="XLabs\ChatBundle\Model\XLabsChatUserInterface", inversedBy="receivedMessages")
     * @ORM\JoinColumn(name="destinatary_id", referencedColumnName="id")
     **/
    /*protected $destinatary;

    public function getReceiver()
    {
        return $this->destinatary;
    }

    public function setReceiver($receiver)
    {
        $this->destinatary = $receiver;
    }*/

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ChatBundle\Model\XLabsChatUserInterface")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     **/
    protected $author;

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    private $creationdate;

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    /*
     * @ORM\Column(name="message_type", type="string", length=5, nullable=false)
     */
    /*protected $message_type;

    public function getMessageType()
    {
        return $this->message_type;
    }

    public function setMessageType($message_type)
    {
        $this->message_type = $message_type;
    }*/

    /**
     * @ORM\ManyToOne(targetEntity="Conversation", inversedBy="messages")
     * @ORM\JoinColumn(name="conversation_id", referencedColumnName="conversation_id")
     */
    protected $conversation;

    public function getConversation()
    {
        return $this->conversation;
    }

    public function setConversation($conversation)
    {
        $this->conversation = $conversation;
    }

    /**
     * @ORM\OneToMany(targetEntity="XLabs\ChatBundle\Entity\MessageRecipient", mappedBy="message", cascade={"remove"})
     */
    protected $recipients;

    public function getRecipients()
    {
        return $this->recipients;
    }

    public function addRecipient($recipient)
    {
        $this->recipients->add($recipient);
    }

    public function removeRecipient($recipient)
    {
        $this->recipients->removeElement($recipient);
    }

    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->creationdate = new DateTime();
    }

    public function getXLabsResultCacheKeyForConversation()
    {
        return array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration',
            Conversation::RESULT_CACHE_ITEM_PREFIX.$this->getConversation()->getConversationId(),
            Conversation::RESULT_CACHE_ITEM_PREFIX.$this->getConversation()->getConversationId().'_hydration'
        );
    }
}