<?php

namespace XLabs\ChatBundle\Repository;

use Doctrine\ORM\EntityRepository;
use \DateTime;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use XLabs\ChatBundle\Entity\XLabsChatUser;

class XLabsChatUserRepository extends EntityRepository
{
    public function getXLabsChatUser($user_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->select('xlcu')
            //->addSelect('partial xlcu_u.{id, username, usernameCanonical}')
            ->addSelect('xlcu_u')
            ->addSelect('partial xlcu_bu.{user}')
            ->from('XLabsChatBundle:XLabsChatUser', 'xlcu')
            ->join('xlcu.user', 'xlcu_u')
            ->leftjoin('xlcu.blocked_users', 'xlcu_bu')
            ->where(
                $qb->expr()->eq('xlcu_u.id', $user_id)
            );
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getTotalOnlineUsers()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->select('COUNT(DISTINCT xlcu.user) AS total_online_users')
            ->from('XLabsChatBundle:XLabsChatUser', 'xlcu')
            ->join('xlcu.user', 'xlcu_u')
            ->where(
                $qb->expr()->eq('xlcu.status', XLabsChatUser::XLABS_CHAT_USER_STATUS_ONLINE)
            );
        $total_online_users = $qb->getQuery()->getArrayResult();

        if($total_online_users)
        {
            return $total_online_users[0]['total_online_users'];
        }
        return 0;
    }

    /*public function getXLabsChatUsers($aOptions)
    {
        $default_options = array(
            'status' => array(
                XLabsChatUser::XLABS_CHAT_USER_STATUS_ONLINE,
                XLabsChatUser::XLABS_CHAT_USER_STATUS_OFFLINE,
                XLabsChatUser::XLABS_CHAT_USER_STATUS_IDLE
            ),
            'roles' => array(
                'ROLE_USER'
            ),
        );
        $aOptions = array_merge($default_options, $aOptions);

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $orX = $qb->expr()->orX();
        foreach($aOptions['roles'] as $role)
        {
            $orX->add($qb->expr()->like('xlcu_u.roles', $qb->expr()->literal('%'.$role.'%')));
        }

        $qb
            ->select('xlcu')
            ->addSelect('partial xlcu_u.{id}')
            ->from('XLabsChatBundle:XLabsChatUser', 'xlcu')
            ->join('xlcu.user', 'xlcu_u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->in('xlcu.status', $aOptions['status']),
                    $orX
                )
            )
            ->getQuery()
            ->getArrayResult();

        $users = array();
        array_walk($_users, function (&$value, $key) use (&$users) {
            $users[$value['id']] = array(
                'id' => $value['id'],
                'username' => $value['username'],
                'canonical' => $value['usernameCanonical'],
                'avatar' => 'https://cdn.static.stiffia.com/media/bikinifanatics/'.$value['folder'].'/'.$value['profileImage']
            );
        });
        return $users;
    }*/
}