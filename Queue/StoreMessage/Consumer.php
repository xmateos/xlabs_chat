<?php

namespace XLabs\ChatBundle\Queue\StoreMessage;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use XLabs\ChatBundle\Entity\Conversation;
use XLabs\ChatBundle\Entity\Message;
use XLabs\ChatBundle\Entity\MessageRecipient;
use \DateTime;

class Consumer extends Parent_Consumer
{
    protected static $consumer = 'xlabs_chat:message:store';

    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    public function getQueueName()
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $chat_settings = $container->getParameter('xlabs_chat_config');
        return 'chat_store_message';
    }

    public function callback($msg)
    {
        // this is all sample code to output something on screen
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine')->getManager();
        $xlabs_chat_config = $container->getParameter('xlabs_chat_config');

        $msg = json_decode($msg->body);

        $conversation_id = $msg->conversation_id;
        $sender_id = $msg->sender_id;
        $participants = explode(',', $msg->participants);
        $participants = array_unique($participants);
        $body = $msg->body;
        $timestamp = $msg->timestamp;

        // check conversation exists
        $conversation = $em->getRepository('XLabsChatBundle:Conversation')->findOneBy(array(
            'conversation_id' => $conversation_id
        ));
        if(!$conversation)
        {
            $conversation = new Conversation();
            $conversation->setConversationId($conversation_id);

            // if private chat, set title to destination username
            /*if(count($participants) == 2)
            {
                $aParticipants = $participants;
                if(($key = array_search($sender_id, $aParticipants)) !== false) {
                    unset($aParticipants[$key]);
                }
                array_filter($aParticipants);
                $destinatary_id = $aParticipants[0];

                $destinatary = $em->getRepository($xlabs_chat_config['user_entity'])->findOneBy(array(
                    'id' => $destinatary_id
                ));
                $conversation->setTitle($destinatary->getUsername());
            }*/

            foreach($participants as $participant)
            {
                $participant = $em->getReference($xlabs_chat_config['user_entity'], $participant);
                $conversation->addParticipant($participant);
            }

            // Check if it´s a private or group conversation
            $_participants = $conversation->getParticipants()->filter(function($participant) use ($sender_id) {
                return $participant->getId() != $sender_id;
            });
            if($_participants->count() == 1)
            {
                $conversation->setConversationType($conversation::XLABS_CHAT_CONVERSATION_TYPE_USER);
            } else {
                $conversation->setConversationType($conversation::XLABS_CHAT_CONVERSATION_TYPE_GROUP);
            }
        }

        $conversation->setLastupdate(new DateTime());
        $em->persist($conversation);
        $em->flush();

        $author = $em->getReference($xlabs_chat_config['user_entity'], $sender_id);

        $message = new Message();
        $message->setAuthor($author);
        $message->setBody($body);
        $message->setSubject('');
        $message->setConversation($conversation);
        $em->persist($message);

        foreach($participants as $participant)
        {
            $participant = $em->getReference($xlabs_chat_config['user_entity'], $participant);
            if($participant->getId() != $author->getId())
            {
                $message_recipient = new MessageRecipient();
                $message_recipient->setMessage($message);
                $message_recipient->setDestinatary($participant);
                $em->persist($message_recipient);
            }
            //$message->addRecipient();
        }

        $em->flush();

        /*foreach($participants as $participant)
        {
            $message = new Message();
            $message->setAuthor($em->getReference($xlabs_chat_config['user_entity'], $sender_id));
            $message->setDestinatary($em->getReference($xlabs_chat_config['user_entity'], $participant));
            $message->setBody($body);
            $message->setSubject('');
            $message->setConversation($conversation);
            $em->persist($message);
        }
        $em->flush();*/

        //dump($msg);
    }
}