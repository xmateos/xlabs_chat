<?php

namespace XLabs\ChatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ChatController extends Controller
{
    public function loaderAction()
    {
        $bundle_config = $this->getParameter('xlabs_chat_config');
        $request = $this->get('request_stack')->getParentRequest();

        $token = $this->get('security.token_storage')->getToken();

        //$isLoggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $isLoggedIn = $token instanceof UsernamePasswordToken;
        //$chat_enabled = $this->get('xlabs_chat')->isAlive();
        $isChatHomepage = $request->get('_route') == 'xlabs_chat_homepage';

        if(!$isLoggedIn || $isChatHomepage)
        {
            $response = new Response('');
            /*$response = new Response(json_encode(array(
                'error' => true
            )), 200);*/
        } else {
            $user = $token->getUser();
            $conversations = $this->get('xlabs_chat_conversations')->getUserConversations(array(
                'user_id' => $user->getId()
            ));
            $total_unread_messages = 0;
            array_map(function($conversation) use (&$total_unread_messages){
                $total_unread_messages += $conversation['total_unread_messages'];
            }, $conversations);

            $response = $this->render('XLabsChatBundle:Frontend:loader.html.twig', array(
                'config' => $bundle_config,
                'total_unread_messages' => $total_unread_messages,
                'channel_name' => md5(json_encode($bundle_config))
            ));
            /*$response = new Response(json_encode(array(
                'error' => false
            )), 200);*/
        }

        //$response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function indexAction()
    {
        if(!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->render('XLabsChatBundle:Frontend:error.html.twig', array(
                'error_description' => 'Chat is only available for members'
            ));
        }

        $bundle_config = $this->getParameter('xlabs_chat_config');

        /*$chat_enabled = $this->get('xlabs_chat')->isAlive();
        if(!$chat_enabled)
        {
            return $this->render('XLabsChatBundle:Frontend:error.html.twig', array(
                'error_description' => 'The chat is currently disabled.'
            ));
        }*/

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $xlc_user = $em->getRepository('XLabsChatBundle:XLabsChatUser')->getXLabsChatUser($user->getId());
        $user_is_banned = $xlc_user->isBanned();
        if($user_is_banned)
        {
            return $this->render('XLabsChatBundle:Frontend:error.html.twig', array(
                'error_description' => 'You have been banned from the chat.'
            ));
        }

        $conversations = $this->get('xlabs_chat_conversations')->getUserConversations(array(
            'user_id' => $user->getId()
        ));
        //if($this->container->getParameter('kernel.environment') == 'dev')
        //{

        //}

        // Online users
        $online_users = $this->get('xlabs_chat')->getOnlineUsers();
        if(($key = array_search($user->getId(), $online_users['online_user_ids'])) !== false) {
            unset($online_users['online_user_ids'][$key]);
        };

        // User´s blocked users
        $blocked_users = $xlc_user->getBlockedUsers()->map(function($v){
            return $v->getUser()->getId();
        })->toArray();

        // Full users list
        $users = $this->get('xlabs_chat_user_manager')->getUsers(array(
            'include_banned' => true
        ));
        // Exclude current user from list
        unset($users[$user->getId()]);

        // Check if specific conversation should be loaded by default
        $request = $this->get('request_stack')->getCurrentRequest();
        $chat_username = $request->get('u');
        $chat_user = $chat_username ? $this->get('xlabs_chat_user_manager')->getUser($chat_username, $user->getId()) : false;

        return $this->render('XLabsChatBundle:Client:index.html.twig', array(
            'online_users' => json_encode($online_users),
            'conversations' => json_encode($conversations),
            'blocked_users' => json_encode($blocked_users),
            'config' => $bundle_config,
            'users' => json_encode($users),
            'chat_user' => $chat_user,
            'channel_name' => md5(json_encode($bundle_config))
        ));
    }

    public function getConversationMessagesAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();

        $conversation_id = $request->request->get('conversation_id');
        $last_message_id = $request->request->get('last_message_id');

        $messages = $this->get('xlabs_chat_messages')->getConversationMessages(array(
            'conversation_id' => $conversation_id,
            'max_results' => 6,
            'last_message_id' => $last_message_id ? $last_message_id : false
        ));

        $response = new Response(json_encode($messages), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function markAsReadAction()
    {
        //$bundle_config = $this->getParameter('xlabs_chat_config');

        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $conversation_id = $request->request->get('conversation_id');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('mr')
            ->from('XLabsChatBundle:MessageRecipient', 'mr')
            ->join('mr.destinatary', 'mr_d', 'WITH', $qb->expr()->eq('mr_d.id', $user->getId()))
            ->join('mr.message', 'mr_m')
            ->join('mr_m.conversation', 'mr_m_c', 'WITH', $qb->expr()->eq('mr_m_c.conversation_id', $qb->expr()->literal($conversation_id)))
            ->where(
                $qb->expr()->eq('mr.read_receipt', 0)
            );
        $message_recipients = $qb->getQuery()->getResult();

        foreach($message_recipients as $message_recipient)
        {
            $message_recipient->setRead(true);
            $em->persist($message_recipient);
        }
        $em->flush();

        //$response = new Response(json_encode($data), 200);
        //$response->headers->set('Content-Type', 'application/json');
        return new Response(count($message_recipients));
    }

    public function uploadImageAction()
    {
        $bundle_config = $this->getParameter('xlabs_chat_config');

        $data = array();
        if(isset($_FILES) && !empty($_FILES))
        {
            $error = false;
            $files = array();

            $web_folder = $this->get('kernel')->getRootDir().'/../web/';
            $uploaddir = rtrim($bundle_config['uploads']['folder'], '/').'/';
            $web_uploaddir = $web_folder.$uploaddir;
            if(!is_dir($uploaddir))
            {
                mkdir($uploaddir, 0777, true);
            }

            foreach($_FILES as $file)
            {
                $uploaded_file = pathinfo($file['name']);
                $uniq_filename = md5(basename($file['name']).time()).'.'.$uploaded_file['extension'];
                if(in_array($uploaded_file['extension'], $bundle_config['uploads']['allowed_extensions']))
                {
                    if(move_uploaded_file($file['tmp_name'], $web_uploaddir.$uniq_filename))
                    {
                        @chmod($web_uploaddir.$uniq_filename, 0777);
                        $files[] = $uploaddir.$uniq_filename;
                    }
                    else
                    {
                        $error = 'There was an error uploading your files';
                    }
                } else {
                    $error = 'File extension not allowed';
                }
            }
            $data = ($error) ? array('error' => $error) : array('files' => $files);
        } else {
            $data = array(
                'error' => 'No file has been selected'
            );
        }

        $response = new Response(json_encode($data), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function toggleBlockUserAction()
    {
        $bundle_config = $this->getParameter('xlabs_chat_config');

        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $blocked_user_id = $request->request->get('blocked_uid');
        if(!$blocked_user_id)
        {
            $response = new Response(json_encode(array(
                'error' => 'No user was specified'
            )), 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $xlabs_chat_user = $em->getRepository('XLabsChatBundle:XLabsChatUser')->getXLabsChatUser($user->getId());
        $xlabs_chat_blocked_user = $em->getReference('XLabsChatBundle:XLabsChatUser', $blocked_user_id);

        if($xlabs_chat_user->getBlockedUsers()->contains($xlabs_chat_blocked_user))
        {
            $xlabs_chat_user->removeBlockedUser($xlabs_chat_blocked_user);
            $blocked_status = 0;
        } else {
            $xlabs_chat_user->addBlockedUser($xlabs_chat_blocked_user);
            $blocked_status = 1;
        }
        $em->persist($xlabs_chat_user);
        $em->flush();

        $response = new Response(json_encode(array(
            'blocked_uid' => $blocked_user_id,
            'status' => $blocked_status
        )), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /*
     * Not in use yet
     */
    public function toggleBanUserAction()
    {
        $bundle_config = $this->getParameter('xlabs_chat_config');

        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $banned_user_id = $request->request->get('banned_user_id');
        if(!$banned_user_id)
        {
            $response = new Response(json_encode(array(
                'error' => 'No user was specified'
            )), 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $xlabs_chat_user = $em->getRepository('XLabsChatBundle:XLabsChatUser')->getXLabsChatUser($banned_user_id);
        $xlabs_chat_user->setBanned(!$xlabs_chat_user->isBanned());
        $em->persist($xlabs_chat_user);
        $em->flush();

        $response = new Response(json_encode(array(
            'banned_user_id' => $banned_user_id,
            'status' => (int)$xlabs_chat_user->isBanned()
        )), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function reportConversationAction()
    {
        $bundle_config = $this->getParameter('xlabs_chat_config');

        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $conversation_id = $request->request->get('conversation_id');
        if(!$conversation_id)
        {
            $response = new Response(json_encode(array(
                'error' => 'No conversation was specified'
            )), 200);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $messages = $this->get('xlabs_chat_messages')->getConversationMessages(array(
            'conversation_id' => $conversation_id
        ));
        if($messages)
        {
            $mail_body = $this->render('XLabsChatBundle:Mail:report.html.twig', array(
                'reporter' => $user,
                'conversation' => $this->get('xlabs_chat_conversations')->getConversationById($conversation_id),
                'messages' => array_reverse($messages['messages'])
            ));
        }

        $headers = '';
        /*$headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";
        $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
        $headers .= "CC: susan@example.com\r\n";*/
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if($bundle_config['settings']['report_to'] && is_array($bundle_config['settings']['report_to']))
        {
            foreach($bundle_config['settings']['report_to'] as $recipient)
            {
                mail($recipient, 'Chat conversation reported', $mail_body, $headers);
            }
        }

        $response = new Response(json_encode(array(
            'conversation_id' => $conversation_id
        )), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function reloadUsersJSONAction()
    {
        // Full users list (like on indexAction)
        $users = $this->get('xlabs_chat_user_manager')->getUsers(array(
            'include_banned' => true
        ));
        // Exclude current user from list
        //$user = $this->get('security.token_storage')->getToken()->getUser();
        //unset($users[$user->getId()]);

        $response = new Response(json_encode($users), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}