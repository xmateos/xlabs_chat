<?php

namespace XLabs\ChatBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use XLabs\ChatBundle\Entity\Conversation;
use XLabs\ChatBundle\Entity\Message;

class MessageRepository
{
    private $em;
    private $config;

    public function __construct(EntityManager $em, $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    public function getConversationMessages($aParams)
    {
        $default_params = array(
            'conversation_id' => false,
            'max_results' => false,
            'last_message_id' => false // for 'show older' pagination
        );
        $aParams = array_merge($default_params, $aParams);

        $em = $this->em;
        $conversation_id = $aParams['conversation_id'];

        // Total messages
        $qb = $em->createQueryBuilder();
        $qb
            ->select($qb->expr()->count('DISTINCT m.id'))
            ->from('XLabsChatBundle:Message','m')
            ->join('m.conversation', 'm_c', 'WITH', $qb->expr()->eq('m_c.conversation_id', $qb->expr()->literal($conversation_id)));
        $total_messages = $qb->getQuery()->getArrayResult();

        // Actual messages
        $qb = $em->createQueryBuilder();
        $qb
            ->select('m.id')
            ->from('XLabsChatBundle:Message','m')
            ->join('m.conversation', 'm_c', 'WITH', $qb->expr()->eq('m_c.conversation_id', $qb->expr()->literal($conversation_id)))
            ->orderBy('m.creationdate','DESC')
            ->addOrderBy('m.id', 'DESC');

        if($aParams['last_message_id'])
        {
            $qb
                ->andWhere(
                    $qb->expr()->lt('m.id', $aParams['last_message_id'])
                );
        }

        $query = $qb->getQuery();

        if($aParams['max_results'])
        {
            $query->setMaxResults($aParams['max_results']);
        }

        $aIds = $query->getArrayResult();
        $aIds = array_map(function($v){
            return $v['id'];
        }, $aIds);

        $results = array(
            'total_messages' => $total_messages[0][1],
            'messages' => $this->getMessagesById(array(
                'message_ids' => $aIds,
                'sorting' => 'field'
            ))
        );
        return $results;
    }

    public function getMessagesById($aParams)
    {
        $default_params = array(
            'message_ids' => false,
            'sorting' => 'date',
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['message_ids'] || empty($aParams['message_ids']))
        {
            return array();
        }

        $qb = $this->em->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT m.id')
            ->from('XLabsChatBundle:Message','m');

        if(is_array($aParams['message_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('m.id', $aParams['message_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('m.id', $aParams['message_ids']));
        }

        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'date':
                $aIds_qb->orderBy('m.creationdate','DESC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['message_ids'];
                break;
        }

        $results = array();
        foreach($aIds as $message_id)
        {
            $results[] = $this->getMessageById($message_id);
        }
        return array_filter($results);
    }

    public function getMessageById($message_id)
    {
        $mappings = $this->config['user_entity_mappings'];
        $em = $this->em;
        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial m.{id, body, creationdate}')
            ->addSelect('partial m_a.{'.$mappings['id'].', '.$mappings['username'].', '.$mappings['avatar'].'}')
            //->addSelect('partial m_a.{'.$mappings['id'].' AS id, '.$mappings['username'].' AS username, '.$mappings['avatar'].' AS avatar}')
            ->from('XLabsChatBundle:Message','m')
            ->join('m.author', 'm_a')
            ->where($qb->expr()->eq('m.id', $message_id));

        $query = $qb->getQuery();

        $cache = $em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(Message::RESULT_CACHE_ITEM_TTL, Message::RESULT_CACHE_ITEM_PREFIX.$message_id.'_hydration', $cache);

        $message = $query
            ->useQueryCache(true)
            ->setResultCacheLifetime(Message::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Message::RESULT_CACHE_ITEM_PREFIX.$message_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();
        if($message)
        {
            $message = $message[0];
            $message['creationdate'] = $message['creationdate']->getTimestamp();

            return $message;
        } else {
            $em->getConfiguration()->getResultCacheImpl()->delete(Message::RESULT_CACHE_ITEM_PREFIX.$message_id);
            $em->getConfiguration()->getResultCacheImpl()->delete(Message::RESULT_CACHE_ITEM_PREFIX.$message_id.'_hydration');
            return false;
        }
    }
}