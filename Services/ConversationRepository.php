<?php

namespace XLabs\ChatBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use XLabs\ChatBundle\Entity\Conversation;

class ConversationRepository
{
    private $em;
    private $config;

    public function __construct(EntityManager $em, $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    public function getConversations($aParams)
    {
        $default_params = array(
            'conversation_ids' => false,
            'sorting' => 'date',
            'user_id' => false
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['conversation_ids'] || empty($aParams['conversation_ids']))
        {
            return array();
        }

        $qb = $this->em->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT c.conversation_id AS id')
            ->from('XLabsChatBundle:Conversation','c');

        if(is_array($aParams['conversation_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('c.conversation_id', $aParams['conversation_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('c.conversation_id', $aParams['conversation_ids']));
        }

        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'date':
                $aIds_qb->orderBy('c.lastupdate','DESC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['conversation_ids'];
                break;
        }

        $results = array();
        foreach($aIds as $conversation_id)
        {
            $results[] = $this->getConversationById($conversation_id, $aParams['user_id']);
        }
        return array_filter($results);
    }

    public function getUserConversations($aParams)
    {
        $default_params = array(
            'user_id' => false,
            'sorting' => 'date'
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['user_id'])
        {
            return array();
        }

        $qb = $this->em->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT c.conversation_id AS id')
            ->from('XLabsChatBundle:Conversation','c');

        $aIds_qb->join('c.participants', 'c_p', 'WITH', $aIds_qb->expr()->eq('c_p.id', $aParams['user_id']));

        $aIds = $aIds_qb->getQuery()->getArrayResult();
        $aIds = array_map(function($v){
            return $v['id'];
        }, $aIds);

        return $this->getConversations(array(
            'conversation_ids' => array_filter($aIds),
            'user_id' => $aParams['user_id'],
            'sorting' => $aParams['sorting']
        ));
    }

    /**
     * @param $conversation_id
     * @param bool $user_id -> if set, get also the amount of unread messages for each conversation requested
     * @return array|bool
     */
    public function getConversationById($conversation_id, $user_id = false)
    {
        $mappings = $this->config['user_entity_mappings'];
        $em = $this->em;
        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial c.{conversation_id, title, creationdate, lastupdate, conversation_type}')
            ->addSelect('partial c_p.{'.$mappings['id'].', '.$mappings['username'].', '.$mappings['avatar'].'}')
            ->from('XLabsChatBundle:Conversation','c')
            ->join('c.participants', 'c_p')
            ->where($qb->expr()->eq('c.conversation_id', $qb->expr()->literal($conversation_id)));

        if($user_id)
        {
            $qb_unread_messages = $em->createQueryBuilder();
            $qb_unread_messages
                ->select('COUNT(mr.id)')
                ->from('XLabsChatBundle:MessageRecipient', 'mr')
                ->join('mr.destinatary', 'mr_d')
                ->join('mr.message', 'mr_m')
                ->join('mr_m.conversation', 'mr_m_c', 'WITH', $qb_unread_messages->expr()->eq('mr_m_c.conversation_id', $qb->expr()->literal($conversation_id)))
                ->where(
                    $qb_unread_messages->expr()->andX(
                        $qb_unread_messages->expr()->eq('mr_d', $user_id),
                        $qb_unread_messages->expr()->neq('mr.read_receipt', 1)
                    )
                );
            $qb
                ->addSelect('('.
                    $qb_unread_messages->getQuery()->getDQL()
                    .') AS total_unread_messages');
            //->having($qb->expr()->gte('dvd_videos_count', $aOptions['minimum_videos']));
        }

        // Get last message of conversation (not working, not the way)
        $qb_last_message = $em->createQueryBuilder();
        $qb_last_message
            ->select($qb->expr()->max('m.id'))
            ->from('XLabsChatBundle:Message', 'm')
            ->join('m.conversation', 'm_c', 'WITH', $qb_last_message->expr()->eq('m_c.conversation_id', $qb->expr()->literal($conversation_id)));
        $qb
            ->addSelect('c_m')
            ->leftJoin('c.messages', 'c_m', 'WITH', $qb->expr()->eq('c_m.id', '('.$qb_last_message->getQuery()->getDQL().')'));

        $query = $qb->getQuery();

        $cache = $em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(Conversation::RESULT_CACHE_ITEM_TTL, Conversation::RESULT_CACHE_ITEM_PREFIX.$conversation_id.'_hydration', $cache);

        $conversation = $query
            ->useQueryCache(true)
            ->setResultCacheLifetime(Conversation::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Conversation::RESULT_CACHE_ITEM_PREFIX.$conversation_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();
        if($conversation)
        {
            $conversation = $conversation[0];
            if(array_key_exists('total_unread_messages', $conversation))
            {
                $conversation[0]['total_unread_messages'] = $conversation['total_unread_messages'];
                unset($conversation['total_unread_messages']);
                return $conversation[0];
            }

            return $conversation;
        } else {
            $em->getConfiguration()->getResultCacheImpl()->delete(Conversation::RESULT_CACHE_ITEM_PREFIX.$conversation_id);
            $em->getConfiguration()->getResultCacheImpl()->delete(Conversation::RESULT_CACHE_ITEM_PREFIX.$conversation_id.'_hydration');
            return false;
        }
    }
}