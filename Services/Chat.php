<?php

namespace XLabs\ChatBundle\Services;

class Chat
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /*public function isAlive()
    {
        // PHP < 5.6
        //$fp = @fsockopen(($notifier['schema'] == 'https' ? 'ssl://' : '').$this->config['nodejs_settings']['host'], $this->config['nodejs_settings']['port'], $errno, $errstr);
        // PHP > 5.6
        $stream_context_options = [
            'ssl' => [
                'verify_peer' => true,
                'cafile' => '/usr/lib/ssl/cert.pem',
                //'CN_match' => $notifier['host'], # deprecated in favor of 'peer_name'
                'peer_name' => $this->config['nodejs_settings']['host'],
            ]
        ];
        $stream_context = stream_context_create($stream_context_options);
        $fp = @stream_socket_client('ssl://'.$this->config['nodejs_settings']['host'].':'.$this->config['nodejs_settings']['port'], $errno, $errstr, 10, STREAM_CLIENT_CONNECT, $stream_context);
        return $fp ? true : false;
    }*/

    public function isAlive()
    {
        $url = 'https://'.$this->config['nodejs_settings']['host'].'/get_online_users';

        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        curl_close($handle);
        if($httpCode >= 200 && $httpCode <= 400) {
            return true;
        } else {
            return false;
        }
    }

    public function getOnlineUsers()
    {
        $url = 'https://'.$this->config['nodejs_settings']['host'].'/get_online_users';
        $raw_data = file_get_contents($url);
        return json_decode($raw_data, true);
    }
}