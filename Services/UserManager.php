<?php

namespace XLabs\ChatBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use XLabs\ChatBundle\Entity\XLabsChatUser;

class UserManager
{
    private $config;
    private $container;
    private $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $container->getParameter('xlabs_chat_config');
        $this->em = $container->get('doctrine')->getManager();
    }

    public function getUsers($aOptions = array())
    {
        $default_options = array(
            'include_banned' => true,
        );
        $aOptions = array_merge($default_options, $aOptions);

        $mappings = $this->config['user_entity_mappings'];
        $qb = $this->em->createQueryBuilder();
        /*$_users = $qb
            ->select('partial u.{'.$mappings['id'].', '.$mappings['username'].', '.$mappings['avatar'].'}')
            ->from($this->config['user_entity'], 'u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->neq('u.'.$mappings['username'], $qb->expr()->literal(''))
                )
            )
            ->orderBy('u.'.$mappings['username'], 'ASC');*/
        $_users = $qb
            ->select('xlcu')
            ->addSelect('partial xlcu_u.{'.$mappings['id'].', '.$mappings['username'].', '.$mappings['avatar'].'}')
            ->from('XLabsChatBundle:XLabsChatUser', 'xlcu')
            ->join('xlcu.user', 'xlcu_u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->neq('xlcu_u.'.$mappings['username'], $qb->expr()->literal(''))
                )
            )
            ->orderBy('xlcu_u.'.$mappings['username'], 'ASC');

        if(!$aOptions['include_banned'])
        {
            $_users
                ->andWhere(
                    $qb->expr()->neq('xlcu.banned', 1)
                );
        }

        $_users = $_users
            ->getQuery()
            ->getArrayResult();

        $users = array();
        $images_prefix = $this->config['settings']['images_prefix'] ? $this->config['settings']['images_prefix'] : '';
        array_walk($_users, function (&$value, $key) use (&$users, $mappings, $images_prefix) {
            $users[$value['user']['id']] = array(
                'id' => $value['user'][$mappings['id']],
                'username' => $value['user'][$mappings['username']],
                //'avatar' => 'https://cdn.static.stiffia.com/media/bikinifanatics/'.$value['folder'].'/'.$value['profileImage']
                'avatar' => $value['user'][$mappings['avatar']] ? $images_prefix.$value['user'][$mappings['avatar']] : false,
                'banned' => $value['banned']
            );
        });
        return $users;
    }

    public function getUser($username, $current_user_id)
    {
        $mappings = $this->config['user_entity_mappings'];
        $qb = $this->em->createQueryBuilder();
        $user = $qb
            ->select('partial u.{'.$mappings['id'].', '.$mappings['username'].', '.$mappings['avatar'].'}')
            ->from($this->config['user_entity'], 'u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->neq('u.'.$mappings['id'], $current_user_id),
                    $qb->expr()->eq('u.'.$mappings['username'], $qb->expr()->literal($username))
                )
            )
            ->getQuery()
            ->getArrayResult();
        return $user ? array(
            'id' => $user[0][$mappings['id']],
            'username' => $user[0][$mappings['username']],
            //'avatar' => 'https://cdn.static.stiffia.com/media/bikinifanatics/'.$user[0]['folder'].'/'.$user[0]['profileImage']
            'avatar' => $user[0][$mappings['avatar']]
        ) : false;
    }

    public function searchUsers($aOptions = array())
    {
        $default_options = array(
            'status' => array(
                XLabsChatUser::XLABS_CHAT_USER_STATUS_ONLINE,
                XLabsChatUser::XLABS_CHAT_USER_STATUS_OFFLINE,
                XLabsChatUser::XLABS_CHAT_USER_STATUS_IDLE
            ),
            'roles' => array(
                '{}' // ROLE_USER
            ),
            //'excluded_roles' => array(),
            'max_results' => false,
            'sorting' => 'username',
            'aExcluded_ids' => array(),
        );
        $aOptions = array_merge($default_options, $aOptions);

        $qb = $this->em->createQueryBuilder();

        $orX_roles = $qb->expr()->orX();
        foreach($aOptions['roles'] as $role)
        {
            $orX_roles->add($qb->expr()->like('xlcu_u.roles', $qb->expr()->literal('%'.$role.'%')));
        }

        $_users = $qb
            ->select('xlcu')
            ->from('XLabsChatBundle:XLabsChatUser', 'xlcu')
            ->join('xlcu.user', 'xlcu_u')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->neq('xlcu_u.username', $qb->expr()->literal('')),
                    $qb->expr()->in('xlcu.status', $aOptions['status']),
                    $orX_roles
                )
            );

        if(!empty($aOptions['aExcluded_ids']))
        {
            $_users->andWhere(
                $qb->expr()->notIn('xlcu_u.id', $aOptions['aExcluded_ids'])
            );
        }

        /*if(!empty($aOptions['excluded_roles']))
        {
            foreach($aOptions['roles'] as $role)
            {
                $_users->andWhere(
                    $qb->expr()->notLike('xlcu_u.roles', $qb->expr()->literal('%'.$role.'%'))
                );
            }
        }*/

        /*if($aOptions['exclude_current_user'])
        {
            $_users->andWhere(
                $qb->expr()->neq('xlcu_u.id', $this->container->get('security.token_storage')->getToken()->getUser()->getId())
            );
        }*/

        switch($aOptions['sorting'])
        {
            case 'last_login':
                $_users
                    ->orderBy('xlcu.last_login', 'DESC');
                break;
            case 'username':
                $_users
                    ->orderBy('xlcu_u.username', 'ASC');
                break;
        }

        $_users = $_users->getQuery();

        if($aOptions['max_results'])
        {
            $_users->setMaxResults($aOptions['max_results']);
        }

        $_users = $_users->getArrayResult();

        $users = array();
        array_walk($_users, function (&$value, $key) use (&$users) {
            $users[] = $value['user_id'];
        });
        return $users;
    }
}