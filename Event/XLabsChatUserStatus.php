<?php

namespace XLabs\ChatBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class XLabsChatUserStatus extends Event
{
    const NAME = 'xlabs_chat_user_status.event';

    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($name)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : false;
    }
}