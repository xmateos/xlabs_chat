<?php

namespace XLabs\ChatBundle\Event;
use XLabs\ChatBundle\Event\XLabsChatUserStatus;

class XLabsChatUserOnline extends XLabsChatUserStatus
{
    const NAME = 'xlabs_chat_user_status.online.event';
}