<?php

namespace XLabs\ChatBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateNodeServerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_chat:create:server')
            ->setDescription('Creates NodeJS server file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        $bundle_config = $container->getParameter('xlabs_chat_config');
        $rabbitmq_config = $container->getParameter('xlabs_rabbitmq_config');

        $nodejs_server_source = $container->get('templating')->render('XLabsChatBundle:Server:server.js.twig', array(
            'bundle_config' => $bundle_config,
            'rabbitmq_config' => $rabbitmq_config,
            'channel_name' => md5(json_encode($bundle_config))
        ));

        // Create chat folder
        $chat_folder = 'chat';
        $chat_folder = $container->get('kernel')->getRootDir().'/../web/'.$chat_folder.'/';
        if(!is_dir($chat_folder))
        {
            $oldmask = umask(0);
            mkdir($chat_folder, 0775, true);
            umask($oldmask);
        }

        // Create 'node_modules' folder
        $node_modules_folder = $chat_folder.'node_modules/';
        if(!is_dir($node_modules_folder))
        {
            $oldmask = umask(0);
            mkdir($node_modules_folder, 0775, true);
            umask($oldmask);
        }

        // Copy 'package.json' dependencies file to chat folder
        if(copy($container->get('kernel')->getRootDir().'/../web/bundles/xlabschat/package.json', $chat_folder.'package.json'))
        {
            @chmod($chat_folder.'package.json', 0775);
        } else {
            $output->writeln('Error copying "package.json" dependencies file.');
        }

        // NodeJS server
        $json_file = $chat_folder.'server.js';
        $file = fopen($json_file, 'w');
        fwrite($file, $nodejs_server_source);
        fclose($file);

        $output->writeln('File "'.$json_file.'" generated successfully.');
    }
}