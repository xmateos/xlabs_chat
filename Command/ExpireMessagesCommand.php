<?php

namespace XLabs\ChatBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ExpireMessagesCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('xlabs_chat:messages:expire')
            ->setDescription('Delete expired messages')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();

        $bundle_config = $container->getParameter('xlabs_chat_config');
        $rabbitmq_config = $container->getParameter('xlabs_rabbitmq_config');

        if(!$bundle_config['settings']['message_ttl'])
        {
            exit();
        }

        $em = $container->get('doctrine')->getManager();
        $now =

        $qb = $em->createQueryBuilder();
        $qb
            ->select('m')
            ->from('XLabsChatBundle:Message', 'm')
            ->where(
                $qb->expr()->lte('DATE(m.creationdate)', "DATE_SUB(CURRENT_DATE(), ".$bundle_config['settings']['message_ttl'].", 'hour')")
            );
        $messages = $qb->getQuery()->getResult();

        if($messages)
        {
            foreach($messages as $message)
            {
                $em->remove($message);
            }
            $em->flush();
        }

        // Clear files from uploads folder older than 'message_ttl'
        $web_folder = $container->get('kernel')->getRootDir().'/../web/';
        $uploaddir = rtrim($bundle_config['uploads']['folder'], '/').'/';
        $web_uploaddir = $web_folder.$uploaddir;
        $process = new Process('find '.$web_uploaddir.' -type f -cmin +'.($bundle_config['settings']['message_ttl'] * 60).' -delete');
        $process->run();

        if(!$process->isSuccessful())
        {
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();

        //$output->writeln('File "'.$json_file.'" generated successfully.');
    }
}