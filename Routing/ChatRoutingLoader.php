<?php

namespace XLabs\ChatBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ChatRoutingLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_chat_routing" loader twice');
        }

        $routes = new RouteCollection();
        $aRoutes = array();

        // Main chat frontend loader
        $path = $this->config['loader_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:loader',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_loader';
        $aRoutes[$routeName] = $route;

        // Main chat frontend URL
        $path = $this->config['url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:index',
        );
        /*$requirements = array(
            'parameter' => '\d+',
        );*/
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_homepage';
        $aRoutes[$routeName] = $route;

        // Get conversations
        /*$path = $this->config['get_conversations_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:getConversations',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_get_conversations';
        $aRoutes[$routeName] = $route;*/

        // Get conversation messages
        $path = $this->config['get_conversation_messages_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:getConversationMessages',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_get_conversation_messages';
        $aRoutes[$routeName] = $route;

        // Mark conversation mesages as read (for a recipient)
        $path = $this->config['conversation_messages_mark_as_read_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:markAsRead',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_conversation_messages_mark_as_read';
        $aRoutes[$routeName] = $route;

        // Upload image
        $path = $this->config['upload_image_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:uploadImage',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_upload_image';
        $aRoutes[$routeName] = $route;

        // Block user
        $path = $this->config['block_user_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:toggleBlockUser',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_block_user';
        $aRoutes[$routeName] = $route;

        // Ban user
        $path = $this->config['ban_user_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:toggleBanUser',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_ban_user';
        $aRoutes[$routeName] = $route;

        // Report conversation
        $path = $this->config['report_conversation_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:reportConversation',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_report_conversation';
        $aRoutes[$routeName] = $route;

        // Reload users JSON list
        $path = $this->config['reload_users_json_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:reloadUsersJSON',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_reload_users_json';
        $aRoutes[$routeName] = $route;

        // Join room
        /*$path = $this->config['join_room_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:joinRoom',
        );
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_join_room';
        $aRoutes[$routeName] = $route;*/

        // Leave room
        /*$path = $this->config['leave_room_url'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:leaveRoom',
        );
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_leave_room';
        $aRoutes[$routeName] = $route;*/

        // Send message
        /*$path = $this->config['send_message'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:sendMessage',
        );
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_send_message';
        $aRoutes[$routeName] = $route;*/

        // Get messages
        /*$path = $this->config['get_messages'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:getMessages',
        );
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_get_messages';
        $aRoutes[$routeName] = $route;*/

        // Get members
        /*$path = $this->config['get_members'];
        $defaults = array(
            '_controller' => 'XLabsChatBundle:Chat:getMembers',
        );
        $requirements = array();
        $options = array();
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_chat_get_members';
        $aRoutes[$routeName] = $route;*/

        foreach($aRoutes as $routeName => $route)
        {
            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_chat_routing' === $type;
    }
}