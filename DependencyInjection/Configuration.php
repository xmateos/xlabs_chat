<?php

namespace XLabs\ChatBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_chat');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->scalarNode('user_entity')->isRequired()->end()
                ->arrayNode('user_entity_mappings')->isRequired()
                    ->children()
                        ->scalarNode('id')->end()
                        ->scalarNode('username')->end()
                        //->scalarNode('canonical')->end()
                        ->scalarNode('avatar')->end()
                    ->end()
                ->end()
                ->scalarNode('url')->defaultValue('/chat')->end()
                ->scalarNode('loader_url')->defaultValue('/chat/loader')->end()
                //->scalarNode('get_conversations_url')->defaultValue('/chat/get_conversations')->end()
                ->scalarNode('get_conversation_messages_url')->defaultValue('/chat/get_conversation_messages')->end()
                ->scalarNode('conversation_messages_mark_as_read_url')->defaultValue('/chat/mark_as_read_conversation_messages')->end()
                ->scalarNode('upload_image_url')->defaultValue('/chat/upload_image')->end()
                ->scalarNode('block_user_url')->defaultValue('/chat/block_user')->end()
                ->scalarNode('ban_user_url')->defaultValue('/chat/ban_user')->end()
                ->scalarNode('report_conversation_url')->defaultValue('/chat/report_conversation')->end()
                ->scalarNode('reload_users_json_url')->defaultValue('/chat/users/reload')->end()
                //->scalarNode('join_room_url')->defaultValue('/chat/room/join')->end()
                //->scalarNode('leave_room_url')->defaultValue('/chat/room/leave')->end()
                //->scalarNode('send_message')->defaultValue('/chat/message/send')->end()
                //->scalarNode('get_messages')->defaultValue('/chat/messages/get')->end()
                //->scalarNode('get_members')->defaultValue('/chat/members/get')->end()
                ->arrayNode('nodejs_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('192.168.5.26')->end()
                        ->scalarNode('port')->defaultValue('3000')->end()
                        ->scalarNode('schema')->defaultValue('https')->end()
                        ->scalarNode('ssl_key')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.key')->end()
                        ->scalarNode('ssl_cert')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.crt')->end()
                        ->scalarNode('ssl_bundle')->defaultValue('/etc/nginx/ssl/607842/wildcard.bikinifanatics.com.bundle')->end()
                    ->end()
                ->end()
                ->arrayNode('redis_settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('host')->defaultValue('192.168.5.23')->end()
                        ->integerNode('port')->defaultValue(6379)->end()
                        ->integerNode('database_id')->defaultValue(6)->end()
                        ->scalarNode('_key_namespace')->defaultValue('xlabs:chat')->end()
                    ->end()
                ->end()
                ->arrayNode('rabbitmq_settings')->addDefaultsIfNotSet()
                    ->children()
                        //->scalarNode('host')->isRequired()->end()
                        //->scalarNode('port')->isRequired()->end()
                        //->scalarNode('user')->isRequired()->end()
                        //->scalarNode('password')->isRequired()->end()
                        //->scalarNode('exchange')->isRequired()->end()
                        //->scalarNode('api_port')->isRequired()->end()
                        ->scalarNode('queue_prefix')->defaultValue('')->end()
                    ->end()
                ->end()
                ->arrayNode('uploads')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('folder')->isRequired()->end()
                        ->arrayNode('allowed_extensions')->prototype('scalar')->defaultValue('[\'jpg\', \'jpeg\']')->end()->end()
                        ->scalarNode('max_file_size')->defaultValue('1048576')->end()
                    ->end()
                ->end()
                ->arrayNode('settings')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('message_ttl')->isRequired()->end()
                        ->scalarNode('images_prefix')->isRequired()->end()
                        ->arrayNode('report_to')->prototype('scalar')->defaultValue('[]')->end()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
